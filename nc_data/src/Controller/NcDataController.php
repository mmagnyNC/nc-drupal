<?php

/**
 * @file
 * Contains \Drupal\nc_data\Controller\NcDataController.
 */

namespace Drupal\nc_data\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\node\Entity\Node;
use Drupal\nc_data\NcDataStorage;

class NcDataController extends ControllerBase
{

    public function index()
    {
        $rows = [];
        $headers = [
            "Type de contenu",
            "Nombre de contenu",
            "Action",
        ];

        $fieldsList = [
            'd_content_type',
        ];

        $groupBy = 'd_content_type';

        $orderBy = [
            'd_content_type' => 'ASC',
        ];

        $numPerPage = 25;
        $total = NcDataStorage::load([], $fieldsList, $groupBy, true);
        $page = pager_default_initialize($total, $numPerPage);

        foreach ($entries = NcDataStorage::load([], $fieldsList, $groupBy, false, $page, $numPerPage, $orderBy) as $entry) {
            $contentType = \Drupal::service('entity.manager')->getStorage('node_type')->load($entry->d_content_type);
            $rows[] = [
                'type' => $contentType->get('name') . ' (' . $entry->d_content_type . ')',
                'nb' => $entry->expression,
                'actions' => [
                    'delete' => [
                        '#type' => 'link',
                        '#title' => 'Supprimer',
                        '#url' => Url::fromRoute('nc_data.delete', ['param' => $entry->d_content_type]),
                        '#attributes' => [
                            'class' => ['button', 'button--danger'],
                        ],
                    ],
                ],
            ];
        }

        $build = [
            'actions' => [
                '#type' => 'container',
                '#attributes' => [
                    'class' => ['action-links'],
                ],

                'generate' => [
                    '#type' => 'link',
                    '#title' => 'Générer du contenu',
                    '#url' => Url::fromRoute('nc_data.step1'),
                    '#attributes' => [
                        'class' => ['button', 'button--primary'],
                    ],
                ],
            ],
            'list' => [
                'table-list' => [
                    '#theme' => 'nc_data_list',
                    '#header' => $headers,
                    '#rows' => $rows,
                    '#empty' => "Aucun contenu généré pour le moment",
                ],
            ],
        ];

        return $build;
    }

    public function delete($param)
    {
        $data = NcDataStorage::load(['d_content_type' => $param]);
        $definitions = \Drupal::service('entity_field.manager')->getFieldDefinitions('node', $param);

        if(!empty($data)){
            foreach($data as $node){
                //Node
                $entity = Node::load($node->d_node);

                //Traitement pour les images et les fichiers
                $tabFiles = [];
                foreach($definitions as $name => $definition){
                    switch($definition->getType()){
                        case 'image':
                        case 'file':
                            if(!empty($entity->get($name)->getValue())){
                                foreach ($entity->get($name)->getValue() as $file){
                                    $tabFiles[$file['target_id']] = $file['target_id'];
                                }
                            }
                            break;

                        default:
                            break;
                    }
                }

                //Suppression du node
                $entity->delete($entity);

                //Suppression des lignes relatives au type de contenu
                NcDataStorage::delete([
                    'field' => 'd_id',
                    'value' => $node->d_id,
                ]);

                //Suppression des images et fichiers
                if(!empty($tabFiles)){
                    foreach ($tabFiles as $fid){
                        $fileEntity = File::load($fid);

                        $file_usage = \Drupal::service('file.usage');
                        $list = $file_usage->listUsage($fileEntity);
                        if(empty($list)){
                            $fileEntity->delete();
                        }
                    }
                }
            }

            $message = 'Ce type de contenu a bien été supprimé';
        }else{
            $message = "Erreur : Ce type de contenu n'a pas pu être supprimé";
        }

        return [
            '#markup' => $message,
            'actions' => [
                '#type' => 'container',
                '#attributes' => [
                    'class' => ['action-links'],
                ],

                'button' => [
                    '#type' => 'link',
                    '#title' => "Retour",
                    '#url' => Url::fromRoute('nc_data.admin'),
                    '#attributes' => [
                        'class' => ['button'],
                    ],
                ],
            ],
        ];
    }
}
