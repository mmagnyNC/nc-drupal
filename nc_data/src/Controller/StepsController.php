<?php

/**
 * @file
 * Contains \Drupal\nc_data\Controller\StepsController.
 */

namespace Drupal\nc_data\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Url;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\node\Entity\Node;

use Drupal\nc_data\NcDataStorage;

use Symfony\Component\HttpFoundation\JsonResponse;

class StepsController extends ControllerBase
{

    public function step1()
    {
        $tempstore = \Drupal::service('user.private_tempstore')->get('nc_data');
        $tempstore->delete('nc_data');

        $form_class = '\Drupal\nc_data\Form\Step1Form';
        $build = [
            'form' => \Drupal::formBuilder()->getForm($form_class),
        ];

        return $build;
    }

    public function step2()
    {
        $tempstore = \Drupal::service('user.private_tempstore')->get('nc_data');

        //Content Type
        $contentType = \Drupal::service('entity.manager')->getStorage('node_type')->load($tempstore->get('content-type'));

        //Except list fields
        $tabExcept = [
            'nid',
            'uuid',
            'vid',
            'langcode',
            'type',
            'revision_timestamp',
            'revision_uid',
            'revision_log',
            'status',
            'default_langcode',
            'revision_default',
            'revision_translation_affected',
            'metatag',
            'path',
            'menu_link',

            'promote',
            'sticky',
            'uid',
            'created',
            'changed',

            'rh_action',
            'rh_redirect',
            'rh_redirect_response',
        ];

        //Fields
        $rows = $headers = [];
        $definitions = \Drupal::service('entity_field.manager')->getFieldDefinitions('node', $tempstore->get('content-type'));
        foreach($definitions as $name => $definition){
            if(!in_array($name, $tabExcept)){
                $rows[$name] = [
                    'label'         => $definition->getLabel(),
                    'type'          => $definition->getType(),
                    'required'      => $definition->isRequired(),
                    'multiple'      => $definition->getFieldStorageDefinition()->getCardinality(),
                    'settings'       => $definition->getSettings(),
                ];

                switch($rows[$name]['type']){
                    case 'integer': //NUMBER
                        $rows[$name]['limites'] = [
                            'min' => [
                                '#type' => 'number',
                                '#title' => 'Valeur min',
                                '#attributes' => [
                                    'name' => $name.'-min',
                                    'class' => [
                                        'field-limite',
                                    ],
                                    'value' => (!empty($rows[$name]['settings']['min'])) ? $rows[$name]['settings']['min'] : 0,
                                ],
                            ],

                            'max' => [
                                '#type' => 'number',
                                '#title' => 'Valeur max',
                                '#attributes' => [
                                    'name' => $name.'-max',
                                    'class' => [
                                        'field-limite',
                                    ],
                                    'value' => (!empty($rows[$name]['settings']['max'])) ? $rows[$name]['settings']['max'] : 600,
                                ],
                            ],
                        ];
                        break;

                    case 'text_with_summary': //TEXTAREA
                        $rows[$name]['limites']['max'] = [
                            '#type' => 'number',
                            '#title' => 'Caractères max.',
                            '#attributes' => [
                                'name' => $name.'-max',
                                'class' => [
                                    'field-limite',
                                ],
                                'value' => '600',
                            ],
                        ];
                        break;

                    case 'datetime':
                        if($rows[$name]['settings']['datetime_type'] == 'datetime'){
                            $rows[$name]['limites'] = "Format : Jour et Heure";
                        }else{
                            $rows[$name]['limites'] = "Format : Jour";
                        }
                        break;

                    case 'entity_reference':
                        switch($rows[$name]['settings']['target_type']){
                            case 'taxonomy_term':
                                $entities = [];
                                foreach($rows[$name]['settings']['handler_settings']['target_bundles'] as $target){
                                    $entities[] = Vocabulary::load($target)->get('name');
                                }
                                $entity = 'Taxonomie'. ((count($entities) > 1) ? 's' : '') .' "'. implode(', ', $entities) .'"';
                                break;

                            case 'node':
                                $entities = [];
                                foreach($rows[$name]['settings']['handler_settings']['target_bundles'] as $target){
                                    $entities[] = \Drupal::service('entity.manager')->getStorage('node_type')->load($target)->get('name');
                                }
                                $entity = 'Node'. ((count($entities) > 1) ? 's' : '') .' "'. implode(', ', $entities) .'"';
                                break;

                            default:
                                $entity = '[ENTITE NON PRISE EN CHARGE]';
                                break;
                        }
                        $rows[$name]['limites'] = $entity;
                        break;

                    case 'image':
                        if(!empty($rows[$name]['settings']['default_image']['uuid'])){
                            $file = \Drupal::service('entity.repository')->loadEntityByUuid('file', $rows[$name]['settings']['default_image']['uuid'])->getFileUri();
                            $rows[$name]['limites'] = "Image par défaut : ". $file;
                        }else{
                            $rows[$name]['limites'] = "Image par défaut du module";
                        }
                        break;

                    case 'file':
                        if(!empty($rows[$name]['settings']['default_file']['uuid'])){
                            $file = \Drupal::service('entity.repository')->loadEntityByUuid('file', $rows[$name]['settings']['default_file']['uuid'])->getFileUri();
                            $rows[$name]['limites'] = "Fichier par défaut : ". $file;
                        }else{
                            $rows[$name]['limites'] = "Fichier par défaut du module";
                        }
                        break;

                    default:
                        if(!empty($rows[$name]['settings']['max_length'])){
                            $rows[$name]['limites'] = "Caractères max. : " .$rows[$name]['settings']['max_length'];
                        }
                        break;
                }
            }
        }

        if(!empty($rows)){
            $tempstore->set('fields-list', $rows);

            //HEADER
            $headers = [
                'title' => 'Nom du champ',
                'name' => 'Nom système',
                'type' => 'Type',
                'required' => 'Requis ?',
                'multiple' => 'Multiple ?',
                'limites' => 'Limites',
            ];
        }

        $build = [
            'type' => [
                '#markup' => "<h2>" . $contentType->get('name') ." (". $tempstore->get('content-type') .")</h2>",
            ],
            'fields' => [
                '#theme' => 'nc_data_field_table',
                '#header' => $headers,
                '#rows' => $rows,
                '#empty' => "Aucun champ pour ce type de contenu",
            ],

            'actions' => [
                '#type' => 'container',
                '#attributes' => [
                    'class' => ['action-links'],
                ],

                'button' => [
                    'submit' => [
                        '#type' => 'link',
                        '#title' => "Suivant",
                        '#url' => Url::fromRoute('nc_data.step3'),
                        '#attributes' => [
                            'class' => ['button', 'button--primary'],
                            'id' => 'formFields',
                        ],
                    ],
                    'cancel' => [
                        '#type' => 'link',
                        '#title' => "Annuler",
                        '#url' => Url::fromRoute('nc_data.admin'),
                        '#attributes' => [
                            'class' => ['button'],
                        ],
                    ],
                ],
            ],

            '#attached' => [
                'library' =>  [
                    'nc_data/form',
                ],
            ],
        ];

        return $build;
    }

    public function step2Ajax()
    {
        $tempstore = \Drupal::service('user.private_tempstore')->get('nc_data');
        $data = $tempstore->get('fields-list');

        foreach(\Drupal::request()->request->keys() as $key){
            $tabKey = explode('-', $key);
            $data[$tabKey[0]]['settings'][$tabKey[1]."_length"] = \Drupal::request()->request->get($key);
            $data[$tabKey[0]]['limites'] = [];
            unset($data[$tabKey[0]]['settings']['min']);
            unset($data[$tabKey[0]]['settings']['max']);
        }

        if(!empty($data)){
            $tempstore->set('fields-list', $data);
            $return = true;
        }else{
            $return = false;
        }

        return new JsonResponse($return);
    }

    public function step3()
    {
        $tempstore = \Drupal::service('user.private_tempstore')->get('nc_data');

        //Content Type
        $contentType = \Drupal::service('entity.manager')->getStorage('node_type')->load($tempstore->get('content-type'));

        //Calculs des possibilités
        $tabFields = [];
        $calculPossibility = '';

        foreach($tempstore->get('fields-list') as $key => $field){
            if($field['required'] === true){ //OBLIGATOIRE - COS ou COM
                if($field['multiple'] === -1 || $field['multiple'] > 1){ //COM
                    $tabFields['COM'][$key] = $field;
                }else{ //COS
                    $tabFields['COS'][$key] = $field;
                }
            }else{ //FACULTATIF - CFS ou CFM
                if($field['multiple'] === -1 || $field['multiple'] > 1){ //CFM
                    $tabFields['CFM'][$key] = $field;
                }else{ //CFS
                    $tabFields['CFS'][$key] = $field;
                }
            }
        }

        //COS - Champ Obligatoire Simple
        if(!empty($tabFields['COS'])){
            $calculCOS = pow(1, count($tabFields['COS']));
            if(!empty($calculPossibility)){
                $calculPossibility = $calculPossibility * $calculCOS;
            }else{
                $calculPossibility = $calculCOS;
            }
        }

        //CFS - Champ Facultatif Simple
        if(!empty($tabFields['CFS'])){
            $calculCFS = pow(2, count($tabFields['CFS']));
            if(!empty($calculPossibility)){
                $calculPossibility = $calculPossibility * $calculCFS;
            }else{
                $calculPossibility = $calculCFS;
            }
        }

        //COM - Champ Obligatoire Multiple
        if(!empty($tabFields['COM'])){
            $calculCOM = pow(2, count($tabFields['COM']));
            if(!empty($calculPossibility)){
                $calculPossibility = $calculPossibility * $calculCOM;
            }else{
                $calculPossibility = $calculCOM;
            }
        }

        //CFS - Champ Facultatif Simple
        if(!empty($tabFields['CFM'])){
            $calculCFM = pow(3, count($tabFields['CFM']));
            if(!empty($calculPossibility)){
                $calculPossibility = $calculPossibility * $calculCFM;
            }else{
                $calculPossibility = $calculCFM;
            }
        }

        $tempstore->set('fields-type', $tabFields);

        $tempstore->set('total', $calculPossibility);

        //AFFICHAGE DE LA PAGE
        $build = [
            'type' => [
                '#markup' => "<h2>" . $contentType->get('name') ." (". $tempstore->get('content-type') .")</h2>",
            ],

            'calcul' => [
                '#markup' => "<p>Le module a calculé une possibilité de <b>". $calculPossibility ." contenu". (($calculPossibility > 1) ? 's' : '') ."</b>.</p>
                            <p>Si vous souhaitez générer l'intégralité des contenus, cliquez sur le bouton <b>\"Génération totale\"</b></p>",
            ],

            'partial' => [
                '#markup' => "<p>Si vous ne souhaitez pas tout générer, saisir un nombre dans le champ ci-dessous et cliquez sur le bouton <b>\"Génération partielle\"</b></p>",
                'input' => [
                    '#type' => 'number',
                    '#title' => 'Nombre de contenu à générer',
                    '#attributes' => [
                        'name' => 'partial',
                        'id' => [
                            'fieldPartial',
                        ],
                        'value' => round( $calculPossibility / 2),
                    ],
                ],
            ],

            'hr' => [
                '#markup' => "<hr>",
            ],

            'actions' => [
                '#type' => 'container',
                '#attributes' => [
                    'class' => ['action-links'],
                ],

                'button' => [
                    'submit' => [
                        '#type' => 'link',
                        '#title' => "Génération totale",
                        '#url' => Url::fromRoute('nc_data.step4'),
                        '#attributes' => [
                            'class' => ['button', 'button--primary'],
                            'id' => 'btnSubmit',
                        ],
                    ],
                    'partial' => [
                        '#type' => 'link',
                        '#title' => "Génération partielle",
                        '#url' => Url::fromRoute('nc_data.step4bis'),
                        '#attributes' => [
                            'class' => ['button', 'button--primary'],
                            'id' => 'btnPartial',
                        ],
                    ],
                    'cancel' => [
                        '#type' => 'link',
                        '#title' => "Annuler",
                        '#url' => Url::fromRoute('nc_data.admin'),
                        '#attributes' => [
                            'class' => ['button'],
                        ],
                    ],
                ],
            ],

            '#attached' => [
                'library' =>  [
                    'nc_data/form',
                ],
            ],
        ];

        return $build;
    }

    public function step3Ajax()
    {
        $tempstore = \Drupal::service('user.private_tempstore')->get('nc_data');

        if(!empty(\Drupal::request()->request->get('partial'))){
            $tempstore->set('total', \Drupal::request()->request->get('partial'));
            $return = true;
        }else{
            $return = false;
        }

        return new JsonResponse($return);
    }

    public function step4()
    {
        $tempstore = \Drupal::service('user.private_tempstore')->get('nc_data');

        //Content Type
        $contentType = \Drupal::service('entity.manager')->getStorage('node_type')->load($tempstore->get('content-type'));

        //Fields type
        $fieldsType = $tempstore->get('fields-type');

        //Total
        $total = $tempstore->get('total');

        //Generate contents table
        $tabContents = [];
        if(!empty($fieldsType)){
            for ($i = 1; $i <= $total; $i++) {
                //TYPE
                $tabContents[$i]['type'] = $tempstore->get('content-type');
            }

            //Table COS
            foreach ($fieldsType['COS'] as $fieldname => $value) {
                for ($i = 1; $i <= $total; $i++) {
                    //TREATMENT
                    $tabContents[$i][$fieldname] = $this->treatmentField($value);
                }
            }

            $delta = $total;

            //Table COM
            if(!empty($fieldsType['COM'])){
                $delta = $delta / 2;
                $nbField = 1;
                foreach ($fieldsType['COM'] as $fieldname => $value) {
                    $j = 1;
                    for ($i = 1; $i <= $total; $i++) {
                        $nb = 2;
                        if($j > 0 && $j <= $delta){
                            $nb = 1;
                        }

                        //TREATMENT
                        for ($k = 1; $k <= $nb; $k++){
                            $tabContents[$i][$fieldname][] = $this->treatmentField($value);
                        }
                        if($j >= $delta){
                            $j = 1 - $delta;
                        }else{
                            $j++;
                        }
                    }
                    if($nbField < count($fieldsType['COM'])){
                        $delta = $delta / 2;
                    }
                    $nbField++;
                }
            }

            //Table CFS
            if(!empty($fieldsType['CFS'])){
                $delta = $delta / 2;
                $nbField = 1;
                foreach ($fieldsType['CFS'] as $fieldname => $value) {
                    $j = 1;
                    for ($i = 1; $i <= $total; $i++) {
                        if($j > 0 && $j <= $delta){
                            //TREATMENT
                            $tabContents[$i][$fieldname] = $this->treatmentField($value);
                        }
                        if($j >= $delta){
                            $j = 1 - $delta;
                        }else{
                            $j++;
                        }
                    }
                    if($nbField < count($fieldsType['CFS'])){
                        $delta = $delta / 2;
                    }
                    $nbField++;
                }
            }

            //Table CFM
            if(!empty($fieldsType['CFM'])) {
                $delta = $delta / 3;
                foreach ($fieldsType['CFM'] as $fieldname => $value) {
                    $j = 1;
                    $nb = 1;
                    for ($i = 1; $i <= $total; $i++) {
                        //TREATMENT
                        for ($k = 1; $k <= $nb; $k++) {
                            $tabContents[$i][$fieldname][] = $this->treatmentField($value);
                        }

                        if ($j >= $delta) {
                            $j = 1;
                            if ($nb == 2) {
                                $nb = 0;
                            } else {
                                $nb++;
                            }
                        } else {
                            $j++;
                        }
                    }
                    $delta = $delta / 3;
                }
            }
        }

        $tempstore->set('data', $tabContents);

        //AFFICHAGE DE LA PAGE
        $build = [
            'type' => [
                '#markup' => "<h2>Génération de <span id='numberGenerate'>" . $tempstore->get('total') . "</span> contenu" . (($tempstore->get('total') > 1) ? 's' : '') . " " . $contentType->get('name') ." (". $tempstore->get('content-type') .")</h2>",
            ],

            'loading' => [
                '#markup' => '<h4 id="titleGenerate">0 contenu généré (0%)</h4>
                <progress id="generate" max="' . $tempstore->get('total') . '" value="0"></progress>',
            ],

            'message' => [
                '#markup' => "<div id='messageGenerate'></div>",
            ],

            'actions' => [
                '#type' => 'container',
                '#attributes' => [
                    'class' => ['action-links'],
                ],

                'button' => [
                    'submit' => [
                        '#type' => 'link',
                        '#title' => "Stop",
                        '#url' => Url::fromRoute('nc_data.admin'),
                        '#attributes' => [
                            'class' => ['button', 'button--primary'],
                            'id' => 'stopGenerate',
                        ],
                    ],
                    'cancel' => [
                        '#type' => 'link',
                        '#title' => "Retour à l'administration",
                        '#url' => Url::fromRoute('nc_data.admin'),
                        '#attributes' => [
                            'id' => 'returnGenerate',
                            'style' => 'display: none',
                            'class' => ['button'],
                        ],
                    ],
                ],
            ],

            '#attached' => [
                'library' =>  [
                    'nc_data/progress',
                ],
            ],
        ];

        return $build;
    }

    public function step4Bis()
    {
        $tempstore = \Drupal::service('user.private_tempstore')->get('nc_data');

        //Content Type
        $contentType = \Drupal::service('entity.manager')->getStorage('node_type')->load($tempstore->get('content-type'));

        //Fields type
        $fieldsType = $tempstore->get('fields-type');

        //Total
        $total = $tempstore->get('total');

        //Generate contents table
        $tabContents = [];
        if(!empty($fieldsType)){
            for ($i = 1; $i <= $total; $i++) {
                //TYPE
                $tabContents[$i]['type'] = $tempstore->get('content-type');
            }

            //Table COS
            foreach ($fieldsType['COS'] as $fieldname => $value) {
                for ($i = 1; $i <= $total; $i++) {
                    //TREATMENT
                    $tabContents[$i][$fieldname] = $this->treatmentField($value);
                }
            }

            //Table COM
            if(!empty($fieldsType['COM'])){
                foreach ($fieldsType['COM'] as $fieldname => $value) {
                    for ($i = 1; $i <= $total; $i++) {
                        $nb = 1;
                        if(rand(0, 100) <= 50){
                            $nb = 2;
                        }
                        //TREATMENT
                        for ($j = 1; $j <= $nb; $j++){
                            $tabContents[$i][$fieldname][] = $this->treatmentField($value);
                        }
                    }
                }
            }

            //Table CFS
            if(!empty($fieldsType['CFS'])) {
                foreach ($fieldsType['CFS'] as $fieldname => $value) {
                    for ($i = 1; $i <= $total; $i++) {
                        if (rand(0, 100) <= 50) {
                            //TREATMENT
                            $tabContents[$i][$fieldname] = $this->treatmentField($value);
                        }
                    }
                }
            }

            //Table CFM
            if(!empty($fieldsType['CFM'])) {
                foreach ($fieldsType['CFM'] as $fieldname => $value) {
                    for ($i = 1; $i <= $total; $i++) {
                        if (rand(0, 100) <= 50) {
                            $nb = 1;
                            if (rand(0, 100) <= 50) {
                                $nb = 2;
                            }
                            //TREATMENT
                            for ($j = 1; $j <= $nb; $j++) {
                                $tabContents[$i][$fieldname][] = $this->treatmentField($value);
                            }
                        }
                    }
                }
            }
        }

        $tempstore->set('data', $tabContents);

        //AFFICHAGE DE LA PAGE
        $build = [
            'type' => [
                '#markup' => "<h2>Génération de <span id='numberGenerate'>" . $tempstore->get('total') . "</span> contenu" . (($tempstore->get('total') > 1) ? 's' : '') . " " . $contentType->get('name') ." (". $tempstore->get('content-type') .")</h2>",
            ],

            'loading' => [
                '#markup' => '<h4 id="titleGenerate">0 contenu généré (0%)</h4>
                <progress id="generate" max="' . $tempstore->get('total') . '" value="0"></progress>',
            ],

            'message' => [
                '#markup' => "<div id='messageGenerate'></div>",
            ],

            'actions' => [
                '#type' => 'container',
                '#attributes' => [
                    'class' => ['action-links'],
                ],

                'button' => [
                    'submit' => [
                        '#type' => 'link',
                        '#title' => "Stop",
                        '#url' => Url::fromRoute('nc_data.admin'),
                        '#attributes' => [
                            'class' => ['button', 'button--primary'],
                            'id' => 'stopGenerate',
                        ],
                    ],
                    'cancel' => [
                        '#type' => 'link',
                        '#title' => "Retour à l'administration",
                        '#url' => Url::fromRoute('nc_data.admin'),
                        '#attributes' => [
                            'id' => 'returnGenerate',
                            'style' => 'display: none',
                            'class' => ['button'],
                        ],
                    ],
                ],
            ],

            '#attached' => [
                'library' =>  [
                    'nc_data/progress',
                ],
            ],
        ];

        return $build;
    }

    public function step4Ajax(){
        $tempstore = \Drupal::service('user.private_tempstore')->get('nc_data');
        $nb = \Drupal::request()->query->get('nb');

        //Contents
        $data = $tempstore->get('data');

        //Insert node
        $node = Node::create($data[$nb]);
        $node->save();

        if (is_numeric($node->id())) {
            //Insert on custom table
            $entry = [
                'd_node' => $node->id(),
                'd_content_type' => $data[$nb]['type'],
                'd_date' => time(),
                'd_import' => 1, //A supprimer ?
            ];

            $result = NcDataStorage::insert($entry);

            if($result !== false){
                $intNb = (int)$nb;
                $return = $intNb + 1; // Next
            }else{
                $return = false;
            }
        }else{
            $return = false;
        }

        return new JsonResponse($return);
    }

    private function treatmentField($data){
        $return = [];

        switch($data['type']){
            case 'string':
                $return = trim($this->getString(rand(5, $data['settings']['max_length'])));
                break;

            case 'datetime':
                $return['value'] = $this->getDate(true);
                break;

            case 'entity_reference':
                $tabTargets = [];
                switch($data['settings']['target_type']){
                    case 'taxonomy_term': //TAXONOMY
                        foreach($data['settings']['handler_settings']['target_bundles'] as $vocabulary){
                            $terms =\Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vocabulary);
                            if(!empty($terms)){
                                foreach ($terms as $term) {
                                    $tabTargets[] = $term->tid;
                                }
                            }
                        }
                        break;

                    case 'node':
                        foreach($data['settings']['handler_settings']['target_bundles'] as $node){
                            $nodeEntities = \Drupal::entityQuery('node')->condition('type', $node)->execute();
                            if(!empty($nodeEntities)){
                                foreach ($nodeEntities as $nodeEntity) {
                                    $tabTargets[] = $nodeEntity;
                                }
                            }
                        }
                        break;

                    default:
                        break;
                }

                $randomNumber = rand(0, (count($tabTargets) - 1));
                if(isset($tabTargets[$randomNumber])){
                    $return = [
                        'target_id' => $tabTargets[$randomNumber],
                    ];
                }
                break;

            case 'text_with_summary': //TEXTAREA (WYSIWYG) + SUMMARY
                $return['value'] = "<p>". trim($this->getText(rand(5, $data['settings']['max_length']))) ."</p>";
                $return['format'] = 'full_html';

                if($data['settings']['display_summary'] === true){
                    $return['summary'] = trim($this->getString(rand(0, 255)));
                }
                break;

            case 'integer':
                $return = rand($data['settings']['min_length'], $data['settings']['max_length']);
                break;

            case 'image':
                $return = $this->getImage($data['settings']);
                break;

            case 'file':
                $return = $this->getFile($data['settings']);
                break;

            default:
                break;
        }

        return $return;
    }

    private function getString($length){
        $text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel est vel nisi rutrum ultricies vel ac risus. Praesent dictum mollis nisl, sed placerat lectus ultrices quis. Donec a elit eget tortor tincidunt lacinia. Etiam viverra quam at tincidunt hendrerit.";
        return substr($text, 0, $length);
    }

    private function getText($length){
        $text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel est vel nisi rutrum ultricies vel ac risus. Praesent dictum mollis nisl, sed placerat lectus ultrices quis. Donec a elit eget tortor tincidunt lacinia. Etiam viverra quam at tincidunt hendrerit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel est vel nisi rutrum ultricies vel ac risus. Praesent dictum mollis nisl, sed placerat lectus ultrices quis. Donec a elit eget tortor tincidunt lacinia. Etiam viverra quam at tincidunt hendrerit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel est vel nisi rutrum ultricies vel ac risus. Praesent dictum mollis nisl, sed placerat lectus ultrices quis. Donec a elit eget tortor tincidunt lacinia. Etiam viverra quam at tincidunt hendrerit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel est vel nisi rutrum ultricies vel ac risus. Praesent dictum mollis nisl, sed placerat lectus ultrices quis. Donec a elit eget tortor tincidunt lacinia. Etiam viverra quam at tincidunt hendrerit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel est vel nisi rutrum ultricies vel ac risus. Praesent dictum mollis nisl, sed placerat lectus ultrices quis. Donec a elit eget tortor tincidunt lacinia. Etiam viverra quam at tincidunt hendrerit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel est vel nisi rutrum ultricies vel ac risus. Praesent dictum mollis nisl, sed placerat lectus ultrices quis. Donec a elit eget tortor tincidunt lacinia. Etiam viverra quam at tincidunt hendrerit.";
        return substr($text, 0, $length);
    }

    private function getDate($time){
        if($time === true){
            $format = 'Y-m-d\TH:i:s';
        }else{
            $format = 'Y-m-d';
        }

        $dateDrupal = new DrupalDateTime(date('Y-m-d '. rand(0, 23) .':'. rand(0, 59) .':'. rand(0, 59)));
        $date = $dateDrupal->modify('-'. rand(0, 800) .' day');

        return date($format, $date->getTimestamp());
    }

    private function getImage($default){
        if(!empty($default['default_image']['uuid'])){
            $file = \Drupal::service('entity.repository')->loadEntityByUuid('file', $default['default_image']['uuid']);
            $return = $file->id();
        }else{
            $directory = \Drupal::token()->replace($default['file_directory'], []);
            $filename = 'img-default.jpg';
            $uri = 'public://'.$directory.'/'.$filename;

            //Check image
            $files = \Drupal::entityTypeManager()
                ->getStorage('file')
                ->loadByProperties(['uri' => $uri]);
            $file = reset($files);

            if (!$file) {
                $module_path = \Drupal::service('module_handler')->getModule('nc_data')->getPath();
                $imageDefault = $module_path. '/assets/'.$filename;
                $data = file_get_contents($imageDefault);

                if ($data !== false) {
                    //Creation du répertoire
                    $directory = file_default_scheme() . '://'.$directory;
                    \Drupal::service('file_system')->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);

                    $file = file_save_data($data, $uri, FILE_EXISTS_REPLACE); //A voir si fonction de remplacement
                }
            }

            $return = $file->id();
        }

        return $return;
    }

    private function getFile($default){
        if(!empty($default['default_file']['uuid'])){
            $file = \Drupal::service('entity.repository')->loadEntityByUuid('file', $default['default_file']['uuid']);
            $return = $file->id();
        }else{
            $directory = \Drupal::token()->replace($default['file_directory'], []);
            $filename = 'file-default.pdf';
            $uri = 'public://'.$directory.'/'.$filename;

            //Check image
            $files = \Drupal::entityTypeManager()
                ->getStorage('file')
                ->loadByProperties(['uri' => $uri]);
            $file = reset($files);

            if (!$file) {
                $module_path = \Drupal::service('module_handler')->getModule('nc_data')->getPath();
                $imageDefault = $module_path. '/assets/'.$filename;
                $data = file_get_contents($imageDefault);

                if ($data !== false) {
                    //Creation du répertoire
                    $directory = file_default_scheme() . '://'.$directory;
                    \Drupal::service('file_system')->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);

                    $file = file_save_data($data, $uri, FILE_EXISTS_REPLACE); //A voir si fonction de remplacement
                }
            }

            $return = $file->id();
        }

        return $return;
    }
}
