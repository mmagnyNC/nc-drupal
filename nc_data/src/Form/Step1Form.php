<?php

/**
 * @file
 * Contains \Drupal\nc_data\Form\Step1Form
 */

namespace Drupal\nc_data\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Step 1
 */
class Step1Form extends FormBase
{
    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'nc_data_step1_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        //Content types
        $contentTypes = \Drupal::service('entity.manager')->getStorage('node_type')->loadMultiple();
        $contentTypesList = [];
        foreach ($contentTypes as $contentType) {
            $contentTypesList[$contentType->id()] = $contentType->label();
        }

        $form['types'] = [
            '#type' => 'select',
            '#options' => $contentTypesList,
            '#title' => 'Type de contenu',
            '#default_value' => [],
            '#description' => 'Choisir le type de contenu pour lequel vous souhaitez générer du contenu',
        ];

        $form['actions'] = [
            '#type' => 'container',
            '#attributes' => [
                'class' => ['action-links'],
            ],

            'button' => [
                'submit' => [
                    '#type' => 'submit',
                    '#value' => 'Suivant',
                    '#attributes' => [
                        'class' => ['button', 'button-action', 'button--primary'],
                    ],
                ],
                'cancel' => [
                    '#type' => 'link',
                    '#title' => "Annuler",
                    '#url' => Url::fromRoute('nc_data.admin'),
                    '#attributes' => [
                        'class' => ['button'],
                    ],
                ],
            ],
        ];

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $tempstore = \Drupal::service('user.private_tempstore')->get('nc_data');
        $tempstore->set('content-type', $form_state->getValue('types'));

        $url = Url::fromRoute('nc_data.step2');
        $form_state->setRedirectUrl($url);
    }
}
