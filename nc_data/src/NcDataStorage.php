<?php

namespace Drupal\nc_data;

use \Drupal\Core\Database\Database;

/**
 * Class NcListingStorage.
 */
class NcDataStorage
{
    /**
     * @param array $entry
     * @return bool|\Drupal\Core\Database\StatementInterface|int|null
     */
    public static function insert(array $entry)
    {
        $query = false;
        try {
            $database = \Drupal::database();
            $query = $database->insert('nc_data')
                ->fields($entry)
                ->execute();
        } catch (\Exception $e) { }

        return $query;
    }

    /**
     * @param array $entry
     * @param string $id
     * @return bool|\Drupal\Core\Database\StatementInterface|int|null
     */
    public static function update(array $entry, string $id)
    {
        $query = false;
        try {
            $database = \Drupal::database();
            $query = $database->update('nc_data')
                ->fields($entry)
                ->condition('d_id', $id, '=')
                ->execute();
        } catch (\Exception $e) { }

        return $query;
    }

    /**
     * @param array $entry
     * @return bool|int
     */
    public static function delete(array $entry)
    {
        $query = false;
        try {
            $database = \Drupal::database();
            $query = $database->delete('nc_data')
                ->condition($entry['field'], $entry['value'])
                ->execute();
        } catch (\Exception $e) { }

        return $query;
    }

    /**
     * @param array $conditions
     * @param array $fieldsList
     * @param array $groupBy
     * @param bool $returnCount
     * @param int $page
     * @param int $numPerPage
     * @param array $orderBy
     * @return bool|\Drupal\Core\Database\Query\SelectInterface
     */
    public static function load($conditions = [], $fieldsList = [], $groupBy = '', $returnCount = false, $page = 0, $numPerPage = 50, $orderBy = [])
    {
        $offset = '';
        if($numPerPage > 0){
            $offset = $numPerPage * $page;
        }

        $query = false;
        try {
            $database = \Drupal::database();
            $query = $database->select('nc_data', 'd');


            if(empty($fieldsList)){
                $query->fields('d');
            }else{
                $query->fields('d', $fieldsList);
                $query->addExpression('COUNT(*)');
            }

            foreach ($conditions as $field => $value) {
                $query->condition($field, $value);
            }

            if(!empty($groupBy)){
                $query->groupBy($groupBy);
            }

            if (count($orderBy) > 0) {
                foreach ($orderBy as $field => $direction){
                    $query->orderBy($field, $direction);
                }
            }

            if ($returnCount === true) {
                return $query->countQuery()->execute()->fetchField();
            }else{
                if(!empty($offset)){
                    return $query->range($offset, $numPerPage)->execute()->fetchAll();
                }else{
                    return $query->execute()->fetchAll();
                }
            }
        } catch (\Exception $e) { }

        return $query;
    }

}
