jQuery(document).ready(function($) {
    if($('#formFields').length > 0){
        $('#formFields').on('click', function(e){
            var url = $(this).attr('href');

            e.preventDefault();

            var tabFields = [];
            $('.field-limite').each(function(){
                var obj = {};
                obj['name'] = $(this).attr('name');
                obj['value'] = $(this).val();

                tabFields.push(obj);
            });

            $.ajax({
                url: '/admin/nc-data/step-2-ajax',
                data: tabFields,
                type: 'POST',
            }).done(function(data){
                if(data == true){
                    window.location.href = url;
                }else{
                    alert('ERROR');
                }
            });
        });
    }

    if($('#btnPartial').length > 0){
        $('#btnPartial').on('click', function(e){
            var url = $(this).attr('href');

            e.preventDefault();

            $.ajax({
                url: '/admin/nc-data/step-3-ajax',
                data: {
                    'partial': $('#fieldPartial').val(),
                },
                type: 'POST',
            }).done(function(data){
                if(data == true){
                    window.location.href = url;
                }else{
                    alert('ERROR');
                }
            });
        });
    }
});