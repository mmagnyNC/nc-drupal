jQuery(document).ready(function($) {
    var xhr;

    if($('#generate').length > 0){
        ajaxGenerate(1, $('#generate'), $('#titleGenerate'), $('#numberGenerate').text(), false);
    }

    if($('#stopGenerate').length > 0){
        $('#stopGenerate').on('click', function(e){
            e.preventDefault();
            xhr.abort();

            $('#messageGenerate').html('La génération a été stoppée');
            $('#stopGenerate').hide();
            $('#returnGenerate').show();
        });
    }

    function ajaxGenerate(nb, element, title, total, stop){
        xhr = $.ajax({
            'url': '/admin/nc-data/step-4-ajax',
            'data': {
                'nb': nb,
            },
        }).done(function(res){
            if(res != false){
                var pluriel = 's';
                if((res - 1) <= 1){
                    pluriel = '';
                }
                title.html( (res - 1) + ' contenu' + pluriel + ' généré' + pluriel + ' (' + Math.round(( ((res - 1) * 100) / element.attr('max'))) + '%)' );
                element.val( (res - 1) );

                if(res > parseInt(total)){
                    $('#messageGenerate').html('Génération terminée');
                    $('#stopGenerate').hide();
                    $('#returnGenerate').show();
                }else{
                    ajaxGenerate(res, element, title, total, stop); // Next generation
                }
            }else{
                $('#messageGenerate').html('Une erreur est survenue lors de la génération d\'un contenu, merci de revenir à la page d\'administration');
                $('#stopGenerate').hide();
                $('#returnGenerate').show();
            }
        }).fail(function(){
            $('#messageGenerate').html('Une erreur est survenue lors de la génération, merci de revenir à la page d\'administration');
            $('#stopGenerate').hide();
            $('#returnGenerate').show();
        });
    }
});