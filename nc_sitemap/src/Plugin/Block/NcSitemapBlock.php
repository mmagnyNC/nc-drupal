<?php

namespace Drupal\nc_sitemap\Plugin\Block;

use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\Core\Url;

/**
 * Provides a 'Sitemap' Block.
 *
 * @Block(
 *   id = "nc_sitemap",
 *   admin_label = @Translation("Sitemap - Block"),
 * )
 */
class NcSitemapBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $data = [];
    $tabMenus = [
      'main' => [
        'title' => 'Menu principal',
        'level' => 5,
      ],
      'acces-footer' => [
        'title' => 'Accès rapides',
        'level' => 1,
      ],
      'footer' => [
        'title' => 'Menu pied de page',
        'level' => 1,
      ],
    ];

    foreach ($tabMenus as $id => $level) {
      $menu_tree = \Drupal::menuTree();
      $parameters = new MenuTreeParameters();
      $parameters->setMaxDepth($level['level']);
      $tree = $menu_tree->load($id, $parameters);

      $manipulators = [
        ['callable' => 'menu.default_tree_manipulators:checkAccess'],
        ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
      ];

      $tree = $menu_tree->transform($tree, $manipulators);
      $menu = $menu_tree->build($tree);

      if (!empty($menu['#items'])) {
        $data[$id]['title'] = $level['title'];

        foreach ($menu['#items'] as $key => $item) {
          $entity = \Drupal::service('entity.repository')
            ->loadEntityByUuid('menu_link_content', $item['original_link']->getDerivativeId());
          if ($entity->hasTranslation($language)) {
            $entity = $entity->getTranslation($language);

            $data[$id]['items'][$key] = $this->getLink([
              'title' => $entity->getTitle(),
              'url' => $entity->getUrlObject(),
            ]);

            if (!empty($item['below'])) { // niveau 2
              foreach ($item['below'] as $k => $level2) {
                $entity = \Drupal::service('entity.repository')
                  ->loadEntityByUuid('menu_link_content', $level2['original_link']->getDerivativeId());
                if ($entity->hasTranslation($language)) {
                  $entity = $entity->getTranslation($language);

                  $data[$id]['items'][$key]['children'][$k] = $this->getLink([
                    'title' => $entity->getTitle(),
                    'url' => $entity->getUrlObject(),
                  ]);

                  if (!empty($level2['below'])) { // niveau 3
                    foreach ($level2['below'] as $c => $level3) {
                      $entity = \Drupal::service('entity.repository')
                        ->loadEntityByUuid('menu_link_content', $level3['original_link']->getDerivativeId());
                      if ($entity->hasTranslation($language)) {
                        $entity = $entity->getTranslation($language);

                        $data[$id]['items'][$key]['children'][$k]['children'][$c] = $this->getLink([
                          'title' => $entity->getTitle(),
                          'url' => $entity->getUrlObject(),
                        ]);

                        if (!empty($level3['below'])) { // niveau 4
                          foreach ($level3['below'] as $d => $level4) {
                            $entity = \Drupal::service('entity.repository')
                              ->loadEntityByUuid('menu_link_content', $level4['original_link']->getDerivativeId());
                            if ($entity->hasTranslation($language)) {
                              $entity = $entity->getTranslation($language);

                              $data[$id]['items'][$key]['children'][$k]['children'][$c]['children'][$d] = $this->getLink([
                                'title' => $entity->getTitle(),
                                'url' => $entity->getUrlObject(),
                              ]);

                              if (!empty($level4['below'])) { // niveau 5
                                foreach ($level4['below'] as $e => $level5) {
                                  $entity = \Drupal::service('entity.repository')
                                    ->loadEntityByUuid('menu_link_content', $level5['original_link']->getDerivativeId());
                                  if ($entity->hasTranslation($language)) {
                                    $entity = $entity->getTranslation($language);

                                    $data[$id]['items'][$key]['children'][$k]['children'][$c]['children'][$d]['children'][$e] = $this->getLink([
                                      'title' => $entity->getTitle(),
                                      'url' => $entity->getUrlObject(),
                                    ]);
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

    if (!empty($data)) {
      $build = [
        '#theme' => 'nc_sitemap',
        '#data' => $data,
        '#cache' => [
          'max-age' => 0,
        ],
      ];

      return $build;
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

  private function getLink($item) {
    $data['title'] = $item['title'];

    if ($item['url']->isExternal()) {
      $data['url'] = $item['url']->getUri();
    }
    else {
      if ($item['url']->isRouted() === FALSE) {
        $data['url'] = $item['url']->toString();
      }
      else {
        $data['url'] = Url::fromRoute($item['url']->getRouteName(), $item['url']->getRouteParameters());
      }
    }

    if (!empty($item['below'])) {
      $data['children'] = $this->getChildren($item['below']);
    }

    return $data;
  }

  private function getChildren($items) {
    $data = [];
    foreach ($items as $key => $item) {
      $data[$key] = $this->getLink($item);
    }
    return $data;
  }

}
