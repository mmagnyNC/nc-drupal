CKEDITOR.on('dialogDefinition', function(ev) {
  var dialogName = ev.data.name;
  var dialogDefinition = ev.data.definition;

  if(dialogName == 'table') {
    var info = dialogDefinition.getContents('info');
    info.get('txtWidth')['default'] = '100%';
    info.get('txtBorder')['default'] = '0';
    info.get('txtCellSpace')['default'] = '0';
    info.get('txtCellPad')['default'] = '0';
  }
});

jQuery(document).ready(function($) {
  var orig_allowInteraction = $.ui.dialog.prototype._allowInteraction;
  $.ui.dialog.prototype._allowInteraction = function (event) {
    // address interaction issues with general iframes with the dialog
    if (event.target.ownerDocument != this.document[0]) {
      return true;
    }
    // address interaction issues with dialog window
    if ($(event.target).closest(".cke_dialog").length) {
      return true;
    }
    // address interaction issues with iframe based drop downs in IE
    if ($(event.target).closest(".cke").length) {
      return true;
    }
    return orig_allowInteraction.apply(this, arguments);
  }
});