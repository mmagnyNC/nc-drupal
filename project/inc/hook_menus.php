<?php

function project_theme_suggestions_menu_alter(array &$suggestions, array $variables) {
    if (isset($variables['attributes']['block'])) {
        if (is_array($variables['attributes']['block'])) {
            $block = $variables['attributes']['block'][0];
        } else {
            $block = $variables['attributes']['block'];
        }
        $template = str_replace('-', '_', str_replace('block-', '', $block));
        $suggestions[] = $variables['theme_hook_original'] . '__' . $template;
    }
}