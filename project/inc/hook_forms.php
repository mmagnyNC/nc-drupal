<?php

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Implements hook_form_alter().
 */
function project_form_alter(&$form, FormStateInterface $form_state, $form_id)
{
	if(isset($form['#webform_id']) && !empty($form['#webform_id'])){
		$form['elements']['actions']['cancel'] = [
			'#type' => 'link',
			'#title' => "Retour à l'accueil",
			'#url' => Url::fromRoute('<front>'),
			'#attributes' => [
				'class' => [
					'btn',
					'btn-secondary'
				],
			],
		];
	}

	switch ($form_id) {
		case 'user_pass':
		case 'user_login_form':
			$form['actions']['submit']['#attributes']['class'] = [
				'webform-button--submit',
			];
			break;

		default:
			break;
	}
}
