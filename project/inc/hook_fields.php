<?php

function project_preprocess_image(&$vars) {
	unset($vars['width'], $vars['height'], $vars['attributes']['width'], $vars['attributes']['height']);
}

function project_theme_suggestions_field_alter(array &$suggestions, array $variables) {
	switch($variables['element']['#field_name']){
		case 'field_image':
			$suggestions[] = 'field__node__' . $variables['element']['#field_name'] . '__'. $variables['element']['#view_mode'];
			$suggestions[] = 'field__node__' . $variables['element']['#field_name'] . '__'. $variables['element']['#bundle'] . '__'. $variables['element']['#view_mode'];
			break;

		default:
			break;
	}
}
