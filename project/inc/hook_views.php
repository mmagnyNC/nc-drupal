<?php

function project_theme_suggestions_views_view_alter(array &$suggestions, array $variables) {
	$view = $variables['view'];

	$suggestions[] = 'views_view__' . $view->id() . '_' . $view->current_display;
}

function project_preprocess_views_view(&$variables) {
	unset($variables['#cache']);
	$variables['#cache'] = [
		'max-age' => 0,
	];
	$variables['nc_project'] = [];
	switch($variables['id']) {
		case 'search':
			//Nombre de résultat
			$resultNumber = $variables['header']['result']['#markup'];
			unset($variables['header']['result']);
			$variables['header']['result'] = $resultNumber;

			//Terme recherché
			$variables['header']['message'] = 'résultat'.(($resultNumber > 1) ? 's' : '') . ' à votre recherche';
			$termSearch = \Drupal::request()->query->get('s');
			if (!empty($termSearch)) {
				$variables['header']['message'] = 'résultat'.(($resultNumber > 1) ? 's' : '') . ' pour "' . $termSearch . '"';
			}
			break;

		default:
			break;
	}
}
