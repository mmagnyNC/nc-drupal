<?php
/**
 * Implements template_preprocess_region().
 */
function project_preprocess_region(&$variables) {

    // Add missing region CSS classes.
    $variables['attributes']['class'][] = 'region';

    if (isset($variables['region']) && !empty($variables['region'])) {
        $variables['attributes']['id'] = 'region--' . str_replace('_', '-', $variables['region']);
    }
}