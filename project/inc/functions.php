<?php

use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;

function project_preprocess_pager(&$variables) {
	global $pager_total;

	if(!empty($pager_total))
		$variables['max_pager'] = $pager_total[0];
}

function project_theme_suggestions_page_title_alter(array &$suggestions, array $variables) {
	if ($node = \Drupal::routeMatch()->getParameter('node')) {
		if(is_numeric($node)){
			$node = Node::load($node);
		}

		$content_type = $node->bundle();
		$suggestions[] = 'page_title__'.$content_type;
	}
}

function project_preprocess_file_link(&$variables) {
	if ( !empty($variables['file_size']) ) unset($variables['file_size']);
	if(!empty($variables['link']['#attributes']))
		$variables['link']['#attributes']['target'] = "_blank";
}

function project_form_system_theme_settings_alter(&$form, FormStateInterface &$form_state, $form_id = NULL) {
	$form['theme_settings']['#open'] = false;
	$form['theme_settings']['#weight'] = 97;
	$form['logo']['#open'] = false;
	$form['logo']['#weight'] = 98;
	$form['favicon']['#open'] = false;
	$form['favicon']['#weight'] = 99;

	// OPTIONS
	$form['nc_project'] = [
		'#type' => 'details',
		'#title' => t("Additional options"),
		'#open' => false,
		'#tree' => true,

		// SEO - ROBOTS
		'robots' => [
			'#type' => 'checkbox',
			'#title' => t("Do not show my site to SEO robots"),
			'#default_value' => theme_get_setting('nc_project.robots'),
			'#description' => t("If the box is checked the no-index / no-follow tags will be displayed on the site"),
		],

		// SEO - META DESCRIPTION
		'meta_description' => [
			'#type' => 'textarea',
			'#title' => "Méta description par défaut",
			'#default_value' => (!empty($metaDescription = theme_get_setting('nc_project.meta_description'))) ? $metaDescription : "Venez découvrir notre nouveau site, sur la page [node:title].",
			'#description' => "Le shortcode \"[node:title]\" permettra d'afficher le titre de la page",
		],

		// COOKIES - ANALYTICS
		'analytics' => [
			'#type' => 'textfield',
			'#title' => "Google Analytics UA",
			'#default_value' => theme_get_setting('nc_project.analytics'),
			'#description' => t("Example: UA-XXXXXXXX-X"),
		]
	];
}
