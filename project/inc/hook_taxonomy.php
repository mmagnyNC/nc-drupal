<?php

function project_theme_suggestions_taxonomy_term_alter( array &$suggestions, array $variables ) {
  $suggestions[] = 'taxonomy_term__'. $variables['elements']['name']['#bundle'] .'__'. $variables['elements']['#view_mode'];
}
