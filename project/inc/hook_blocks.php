<?php
/**
 * Implements template_preprocess_region().
 */
function project_preprocess_block(&$variables) {
    // Add missing block CSS classes.
    $variables['attributes']['class'][] = 'block';

    $variables['content']['#attributes']['block'] = $variables['attributes']['id'];

    if (isset($variables['base_plugin_id']) && !empty($variables['base_plugin_id'])) {
        $variables['attributes']['class'][] = 'block--' . str_replace('_', '-', $variables['base_plugin_id']);
    }

    if (isset($variables['plugin_id']) && !empty($variables['plugin_id']) && !empty($variables['attributes']['id'])) {
        $variables['attributes']['id'] = 'block--' . str_replace('_', '-', str_replace(':', '__', $variables['plugin_id']));
    }
}