<?php

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;

function project_preprocess_node(&$variables)
{
	$node = $variables['node'];

	//TITLE pour image et lien
	$variables['title'] = $node->getTitle();

	//TYPE
	$variables['nc_project']['type'] = \Drupal::service('entity_type.manager')->getStorage('node_type')->load($node->bundle())->get('name');

	// Champs vides
	foreach ($variables['content'] as $field => $value){
		if($node->hasField($field) && empty($node->get($field)->getValue())){
			unset($variables['content'][$field]);
		}
	}

	//IMAGE
	if (isset($variables['content']['field_image'])) {
		$variables['content']['field_image'][0]['#item_attributes']['alt'] = $variables['title'];
	} else { // Image par défaut
		$field_info = FieldConfig::loadByName('node', 'page', 'field_image');
		$image_uuid = $field_info->getSetting('default_image')['uuid'];
		$imageDefaut = \Drupal::service('entity.repository')->loadEntityByUuid('file', $image_uuid);
		$variables['nc_project']['image'] = $imageDefaut->get('uri')->getValue()[0]['value'];
	}

	// Par mode d'affichage
	switch ($variables['view_mode']) {
		case 'full':
			switch($node->bundle()){
				default:
					$nidUrl = '';
					break;
			}

			if(!empty($nidUrl)){
				$variables['nc_project']['back'] = Url::fromRoute('entity.node.canonical', ['node' => $nidUrl], ['absolute'=>'true'])->toString();
				$host = \Drupal::request()->getSchemeAndHttpHost();
				if(!empty($referer = $_SERVER['HTTP_REFERER'])) {
					if(str_contains($referer, $host)){
						$variables['nc_project']['back'] = $referer;
					}
				}
			}
			break;

		default:
			break;
	}
}

function project_node_view_alter(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display)
{
	if (empty($build['#attached']['html_head_link'])) {
		return;
	}

	$tags = [
		'delete-form',
		'edit-form',
		'version-history',
		'revision',
		'devel-load',
		'devel-render',
		'devel-definition',
		'latest-version',
		'token-devel',
		'delete-multiple-form',
	];

	foreach ($build['#attached']['html_head_link'] as $key => $tag) {
		if (empty($tag[0]['rel'])) {
			continue;
		}
		if (!in_array($tag[0]['rel'], $tags)) {
			continue;
		}
		// Hide tag.
		if(isset($build['#attached']['html_head_link'][$key])){
			unset($build['#attached']['html_head_link'][$key]);
		}
	}
}
