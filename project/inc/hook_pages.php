<?php

use Drupal\Component\Utility\Unicode;

function project_preprocess_page(&$variables) {
	// Config du site
	$site_config = \Drupal::config('system.site');

	// Site Name
	$variables['site_name'] = $site_config->get('name');
	// Slogan
	$variables['site_slogan'] = $site_config->get('slogan');
	// Logo
	if(empty($variables['site_logo'])){
		$variables['site_logo'] = theme_get_setting('logo.url', 'project');
	}

	//IS LOGIN
	$variables['user_login'] = \Drupal::currentUser()->isAuthenticated();

	$variables['nc_project']['sidebar_first'] = true;
	if(empty(\Drupal::service('renderer')->renderRoot($variables['page']['sidebar_first']))){
		$variables['nc_project']['sidebar_first'] = false;
	}
}

function project_page_attachments_alter(array &$attachments) {
	$descriptionExist = false;
	foreach ($attachments['#attached']['html_head'] as $key => $attachment) {
		if ($attachment[1] == 'system_meta_generator') {
			unset($attachments['#attached']['html_head'][$key]);
		}

		if($attachment[1] == 'description') {
			$descriptionExist = true;
		}
	}

	if($descriptionExist === false){ // META DESCRIPTION INEXISTANTE OU CAS PARTICULIER DE VISUAL EDITOR
		$route_name = \Drupal::routeMatch()->getRouteName();
		$description = theme_get_setting('nc_project.meta_description');

		if($route_name == 'entity.node.canonical'){
			$node = \Drupal::routeMatch()->getParameter('node');
			if (is_object($node)) {
				if(!empty($node->get('body')->getValue())){
					$description = $node->get('body')->getValue()[0]['summary'];
				}
			}
		}

		//TRAITEMENT DES SHORTCODES
		$site_config = \Drupal::config('system.site');
		$nodeTitle = $site_config->get('name');
		if($route_name == 'entity.node.canonical'){
			$node = \Drupal::routeMatch()->getParameter('node');
			if (is_object($node)) {
				$nodeTitle = $node->getTitle();
			}
		}

		if(!empty($description)){
			$description = str_replace('[node:title]', '"'.$nodeTitle.'"', $description);

			$attachments['#attached']['html_head']['description'] = [
				[
					'#tag' => 'meta',
					'#attributes' => [
						'name' => 'description',
						'content' => $description,
					]
				],
				'description',
			];
			$attachments['#attached']['html_head']['og:description'] = [
				[
					'#tag' => 'meta',
					'#attributes' => [
						'name' => 'og:description',
						'content' => $description,
					]
				],
				'og:description',
			];
			$attachments['#attached']['html_head']['twitter:description'] = [
				[
					'#tag' => 'meta',
					'#attributes' => [
						'name' => 'twitter:description',
						'content' => $description,
					]
				],
				'twitter:description',
			];
		}
	} else {
		foreach ( $attachments['#attached']['html_head'] as $key => $attachment ) {
			if ( in_array($attachment[1], ['description', 'og_description', 'twitter_cards_description']) ) {
				$attachments['#attached']['html_head'][$key][0]['#attributes']['content'] = Unicode::truncate($attachment[0]['#attributes']['content'], 170, false, '...');
			}
		}
	}

	foreach ( $attachments['#attached']['html_head'] as $key => $attachment ) {
		// Meta description
		if ( in_array($attachment[1], ['description', 'og:description', 'twitter:description', 'og_description', 'twitter_cards_description']) ) {
			$attachments['#attached']['html_head'][$key][0]['#attributes']['content'] = Unicode::truncate($attachment[0]['#attributes']['content'], 170, false, '...');
		}

		// Title
		if ( in_array($attachment[1], ['og:title', 'twitter:title', 'og_title', 'twitter_cards_title']) ) {
			$attachments['#attached']['html_head'][$key][0]['#attributes']['content'] = Unicode::truncate($attachment[0]['#attributes']['content'], 75, false, '...');
		}
	}

	// Autre balise link
	$html_head_link = $attachments['#attached']['html_head_link'];
	foreach ($html_head_link as $key => $value) {
		if (isset($value[0]['rel'])){
			switch ($value[0]['rel']) {
				case 'delete-form':
				case 'edit-form':
				case 'version-history':
				case 'revision':
				case 'devel-load':
				case 'devel-render':
				case 'devel-definition':
				case 'latest-version':
				case 'token-devel':
				case 'delete-multiple-form':
					unset($attachments['#attached']['html_head_link'][$key]);
					break;
			}
		}
	}
}

function project_theme_suggestions_page_alter( array &$suggestions, array $variables ) {
	$http_error_suggestions = [
		'system.401' => 'page__401',
		'system.403' => 'page__403',
		'system.404' => 'page__404',
	];
	$route_name = \Drupal::routeMatch()->getRouteName();
	if (isset($http_error_suggestions[$route_name])) {
		$suggestions[] = $http_error_suggestions[$route_name];
	}else{
		if($route_name === 'entity.node.canonical'){
			$node = \Drupal::routeMatch()->getParameter('node');
			if(!empty($node)){
				$suggestions[] = 'page__'. $node->bundle();
			}
		}

		if(strpos($route_name, 'view.') !== false){ // VIEW
			$view = \Drupal::routeMatch()->getParameters()->all();
			$suggestions[] = 'page__view__'. $view['view_id'];
			$suggestions[] = 'page__view__'. $view['view_id'] .'__'. $view['display_id'];
		}
	}
}
