<?php

function project_preprocess_html(&$variables){
	$analytics = theme_get_setting('nc_project.analytics');
	if(!empty($analytics))
		$variables['nc_project']['analytics'] = $analytics;
}
