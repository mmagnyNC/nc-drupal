jQuery(window).on("load", function () {
    // Animate loader off screen
    jQuery("body").removeClass("stuck");
    for (var progress = 0 ; progress < 100 ; progress ++){
        jQuery(".progress-bar").css("width",progress+"%");
    }
    setTimeout(function(){
        jQuery("#loader").fadeOut(500);
    }, 700);
});