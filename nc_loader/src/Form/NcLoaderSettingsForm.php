<?php

namespace Drupal\nc_loader\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form that configures forms module settings.
 */
class NcLoaderSettingsForm extends ConfigFormBase {

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'nc_loader_admin_settings';
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames() {
        return [
            'nc_loader.settings',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $config = $this->config('nc_loader.settings');

        $form['type'] = [
            '#type' => 'select',
            '#options' => [
                'default' => $this->t('Default'),
                'bubble' => $this->t('Bubble'),
                'round' => $this->t('Round'),
            ],
            '#title' => $this->t('Loading type'),
            '#default_value' => $config->get('type'),
            '#description' => $this->t('Choose the loading type for the website'),
        ];

        return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $values = $form_state->getValues();
        $this->config('nc_loader.settings')
            ->set('type', $values['type'])
            ->save();

        parent::submitForm($form, $form_state);
    }

}