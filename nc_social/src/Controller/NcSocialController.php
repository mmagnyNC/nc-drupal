<?php

/**
 * @file
 * Contains \Drupal\nc_social\Controller\NcSocialController.
 */

namespace Drupal\nc_social\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\nc_social\NcSocialStorage;

class NcSocialController extends ControllerBase
{

	public function contentList()
	{
		$rows = [];
		$headers = [
			$this->t('Order'),
			$this->t('Label'),
			$this->t('Actions'),
		];
		$orderBy = [
			'sn_order' => 'ASC',
		];

		foreach ($entries = NcSocialStorage::load([], false, 0, 99, $orderBy) as $entry) {
			$fields = [
				'order' => $entry->sn_order,
				'label' => $entry->sn_label,
				'actions' => [
					'edit' => [
						'#type' => 'link',
						'#title' => $this->t('Edit'),
						'#url' => Url::fromRoute('nc_social.edit', ['param' => $entry->sn_id]),
						'#attributes' => [
							'class' => ['button'],
						],
					],
					'delete' => [
						'#type' => 'link',
						'#title' => $this->t('Delete'),
						'#url' => Url::fromRoute('nc_social.delete', ['param' => $entry->sn_id]),
						'#attributes' => [
							'class' => ['button', 'button--danger'],
						],
					],
				]
			];
			$rows[] = $fields;
		}

		return [
			'add' => [
				'#type' => 'container',
				'#attributes' => [
					'class' => ['action-links'],
				],

				'button' => [
					'#type' => 'link',
					'#title' => $this->t('Add a social network'),
					'#url' => Url::fromRoute('nc_social.add'),
					'#attributes' => [
						'class' => ['button', 'button-action', 'button--primary', 'button--small'],
					],
				],
			],

			'table-list' => [
				'#theme' => 'nc_social_admin',
				'#header' => $headers,
				'#rows' => $rows,
				'#empty' => $this->t('No social network at the moment'),
			],

			'pages' => [
				'#type' => 'pager',
			],
		];
	}

	public function delete($param)
	{
		NcSocialStorage::delete(['sn_id' => $param]);

		return [
			'#markup' => $this->t('This social network has been deleted'),
			'actions' => [
				'#type' => 'container',
				'#attributes' => [
					'class' => ['action-links'],
				],

				'button' => [
					'#type' => 'link',
					'#title' => $this->t('Back to list'),
					'#url' => Url::fromRoute('nc_social.admin'),
					'#attributes' => [
						'class' => ['button'],
					],
				],
			],
		];
	}
}
