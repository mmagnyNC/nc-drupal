<?php

namespace Drupal\nc_social;

/**
 * Class NcSocialStorage.
 */
class NcSocialStorage
{

    /**
     * Save an entry in the database.
     */
    public static function insert($entry)
    {
        $query = false;
        try {
            $database = \Drupal::database();
            $query = $database->insert('nc_social')
                ->fields($entry)
                ->execute();
        } catch (\Exception $e) {
            kint($e);
        }
        return $query;
    }

    /**
     * Update an entry in the database.
     */
    public static function update($entry)
    {
        $query = false;
        try {
            $database = \Drupal::database();
            $query = $database->update('nc_social')
                ->fields($entry)
                ->condition('sn_id', $entry['sn_id'])
                ->execute();
        } catch (\Exception $e) {
            kint($e);
        }
        return $query;
    }

    /**
     * Delete an entry from the database.
     */
    public static function delete($entry)
    {
        $query = false;
        try {
            $database = \Drupal::database();
            $database->delete('nc_social')
                ->condition('sn_id', $entry['sn_id'])
                ->execute();
        } catch (\Exception $e) {
            kint($e);
        }
        return $query;
    }

    /**
     * Read from the database using a filter array.
     */
    public static function load($conditions = [], $returnCount = false, $page = 0, $numPerPage = 50, $orderBy = [])
    {
        $offset = '';
        if($numPerPage > 0){
            $offset = $numPerPage * $page;
        }

        $query = false;
        try {
            $database = \Drupal::database();
            $query = $database->select('nc_social', 'nc')
                ->fields('nc');

            if(!empty($conditions)){
                foreach ($conditions as $field => $value) {
                    $query->condition($field, $value);
                }
            }

            if (!empty($orderBy)) {
                foreach ($orderBy as $field => $direction){
                    $query->orderBy($field, $direction);
                }
            }

            if ($returnCount === true)
                return $query->countQuery()->execute()->fetchField();
            else{
                if(!empty($offset)){
                    return $query->range($offset, $numPerPage)->execute()->fetchAll();
                }else{
                    return $query->execute()->fetchAll();
                }
            }
        } catch (\Exception $e) {
            kint($e);
        }
        return $query;
    }
}
