<?php

namespace Drupal\nc_social\Plugin\Block;

use Drupal\nc_social\NcSocialStorage;
use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Social Networks' Block.
 *
 * @Block(
 *   id = "nc_social_block",
 *   admin_label = @Translation("Social Networks - Block"),
 * )
 */
class NcSocialBlock extends BlockBase
{
    /**
     * {@inheritdoc}
     */

    public function build()
    {
        $rows = [];
        $page = 0;
        $num_per_page = 8;
        $orderby = [
            'sn_order' => 'ASC',
        ];

        foreach ($entries = NcSocialStorage::load([], false, $page, $num_per_page, $orderby) as $entry) {
            $fields = [
                'label' => $entry->sn_label,
                'link' => $entry->sn_link,
                'icon' => $entry->sn_icon,
            ];
            $rows[] = $fields;
        }

        if (count($rows) > 0) {
            $build = [
                '#theme' => 'nc_social_list',
                '#data' => $rows,
            ];
        } else {
            $build = [];
        }

        return $build;
    }
}
