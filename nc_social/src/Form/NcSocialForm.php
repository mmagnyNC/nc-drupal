<?php

/**
 * @file
 * Contains \Drupal\nc_social\Form\NcSocialForm
 */

namespace Drupal\nc_social\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\nc_social\NcSocialStorage;

/**
 * Implements a social network admin form
 */
class NcSocialForm extends FormBase
{
    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'nc_social_admin_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, $param = NULL)
    {
        $data = NULL;
        if(!is_null($param)){
            $data = $this->getItem($param);
        }

        $form = [
            'id' => [
                '#type' => 'hidden',
                '#default_value' => $param,
            ],
            'label' => [
                '#type' => 'select',
                '#options' => [
                    'Facebook' => 'Facebook',
                    'Twitter' => 'Twitter',
                    'Google+' => 'Google+',
                    'LinkedIn' => 'LinkedIn',
                    'Instagram' => 'Instagram',
                    'Pinterest' => 'Pinterest',
                    'Youtube' => 'Youtube',
                    'Vimeo' => 'Vimeo',
                ],
                '#title' => t('Label'),
                '#default_value' => (!is_null($data)) ? $data->sn_label : '',
            ],
            'icon' => [
                '#type' => 'select',
                '#options' => [
                    'facebook-f' => 'Facebook',
                    'twitter' => 'Twitter',
                    'google-plus-g' => 'Google+',
                    'linkedin-in' => 'LinkedIn',
                    'instagram' => 'Instagram',
                    'pinterest' => 'Pinterest',
                    'youtube' => 'Youtube',
                    'vimeo-v' => 'Vimeo',
                ],
                '#title' => t('Icon'),
                '#default_value' => (!is_null($data)) ? $data->sn_icon : '',
            ],
            'link' => [
                '#type' => 'textfield',
                '#title' => t('Link'),
                '#default_value' => (!is_null($data)) ? $data->sn_link : '',
            ],
            'order' => [
                '#type' => 'select',
                '#options' => [
                    '1' => '1',
                    '2' => '2',
                    '3' => '3',
                    '4' => '4',
                    '5' => '5',
                    '6' => '6',
                    '7' => '7',
                    '8' => '8',
                ],
                '#title' => t('Order'),
                '#default_value' => (!is_null($data)) ? $data->sn_order : '',
            ],

            'actions' => [
                '#type' => 'container',
                '#attributes' => [
                    'class' => ['action-links'],
                ],

                'button' => [
                    'submit' => [
                        '#type' => 'submit',
                        '#value' => t('Save'),
                        '#attributes' => [
                            'class' => ['button', 'button-action', 'button--primary'],
                        ],
                    ],
                    'cancel' => [
                        '#type' => 'link',
                        '#title' => t('Cancel'),
                        '#url' => Url::fromRoute('nc_social.admin'),
                        '#attributes' => [
                            'class' => ['button'],
                        ],
                    ],
                ],
            ],
        ];

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $data = [
            'sn_label' => $form_state->getValue('label'),
            'sn_link' => $form_state->getValue('link'),
            'sn_icon' => $form_state->getValue('icon'),
            'sn_order' => $form_state->getValue('order'),
        ];
        if(!empty($form_state->getValue('id'))){
            $data['sn_id'] = $form_state->getValue('id');
            $return = NcSocialStorage::update($data);
            $message = 'edited';
        }else{
            $return = NcSocialStorage::insert($data);
            $message = 'added';
        }

        $messenger = \Drupal::messenger();
        if($return > 0){
            $messenger->addMessage($this->t("The social network has been $message"));
        }else{
            $messenger->addMessage($this->t("An error occurred while saving"));
        }

        $url = Url::fromRoute('nc_social.admin');
        $form_state->setRedirectUrl($url);
    }

    private function getItem($entityId)
    {
        $entity = NcSocialStorage::load(['sn_id' => $entityId], false);
        return $entity[0];
    }
}
