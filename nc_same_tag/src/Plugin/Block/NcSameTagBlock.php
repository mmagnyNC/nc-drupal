<?php
namespace Drupal\nc_same_tag\Plugin\Block;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\node\Entity\Node;
use Drupal\Core\Cache\Cache;

/**
 * Provides a 'Same Tag' Block.
 *
 * @Block(
 *   id = "nc_same_tag",
 *   admin_label = @Translation("Same Tag - Block"),
 * )
 */

class NcSameTagBlock extends BlockBase {
    /**
     * {@inheritdoc}
     */
    public function blockForm($form, FormStateInterface $form_state) {
        $form = parent::blockForm($form, $form_state);
        $config = $this->getConfiguration();

        //Récupération des types de contenu du site
        $contentTypes = \Drupal::service('entity.manager')->getStorage('node_type')->loadMultiple();
        $contentTypesList = [];
        foreach ($contentTypes as $contentType) {
            $contentTypesList[$contentType->id()] = $contentType->label();
        }

        //Récupération des champs Entity Reference / Taxonomy
        $tabFieldsTags = $tabFieldsSort = [];
        foreach ($contentTypesList as $key => $type){
            $definitions = \Drupal::service('entity_field.manager')->getFieldDefinitions('node', $key);
            foreach($definitions as $name => $definition){
                switch($definition->getType()){
                    case 'entity_reference':
                        if($definition->getSetting('handler') == 'default:taxonomy_term'){
                            $tabFieldsTags[$name] = $name;
                        }
                        break;

                    case 'datetime':
                    case 'string':
                        $tabFieldsSort[$name] = $name;
                        break;

                    default:
                        break;
                }
            }
        }
        ksort($tabFieldsTags);
        ksort($tabFieldsSort);

        //Récupération des displays des entités
        $viewModes = \Drupal::service('entity_display.repository')->getViewModes('node');
        $tabViewModes = [];
        foreach($viewModes as $id => $viewMode){
            $tabViewModes[$id] = $viewMode['label'];
        }
        asort($tabViewModes);

        $form['nc_same_tag'] = [
            '#type' => 'fieldset',
            '#title' => "Configuration",
            '#tree' => TRUE,

            'field' => [
                '#type' => 'select',
                '#options' => $tabFieldsTags,
                '#title' => $this->t('Field tags name'),
                '#default_value' => isset($config['nc_same_tag_field']) ? $config['nc_same_tag_field'] : null,
                '#description' => $this->t('Select the machine name of the tags field'),
            ],
            'types' => [
                '#type' => 'checkboxes',
                '#options' => $contentTypesList,
                '#title' => $this->t('Content types'),
                '#default_value' => isset($config['nc_same_tag_types']) ? $config['nc_same_tag_types'] : null,
                '#description' => $this->t('Select the desired content type'),
            ],
            'sort' => [
                '#type' => 'select',
                '#options' => $tabFieldsSort,
                '#title' => $this->t('Field sort name'),
                '#default_value' => isset($config['nc_same_tag_sort']) ? $config['nc_same_tag_sort'] : 'title',
                '#description' => $this->t('Enter the machine name of the sort field'),
            ],
            'sortOrder' => [
                '#type' => 'select',
                '#options' => [
                    'ASC' => 'ASC',
                    'DESC' => 'DESC',
                ],
                '#title' => $this->t('Order field sort'),
                '#default_value' => isset($config['nc_same_tag_sortOrder']) ? $config['nc_same_tag_sortOrder'] : 'ASC',
            ],
            'display' => [
                '#type' => 'select',
                '#options' => $tabViewModes,
                '#title' => $this->t('View mode'),
                '#default_value' => isset($config['nc_same_tag_display']) ? $config['nc_same_tag_display'] : 'teaser',
                '#description' => $this->t('Select the machine name of the view mode'),
            ],
        ];
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function blockSubmit($form, FormStateInterface $form_state) {
        parent::blockSubmit($form, $form_state);
        $values = $form_state->getValues();

        $this->configuration['nc_same_tag_field'] = $values['nc_same_tag']['field'];
        $this->configuration['nc_same_tag_types'] = $values['nc_same_tag']['types'];
        $this->configuration['nc_same_tag_sort'] = $values['nc_same_tag']['sort'];
        $this->configuration['nc_same_tag_sortOrder'] = $values['nc_same_tag']['sortOrder'];
        $this->configuration['nc_same_tag_display'] = $values['nc_same_tag']['display'];
    }

    /**
     * {@inheritdoc}
     */
    public function build()
    {
        $contents = [];
        $node = '';

        $route_name = \Drupal::routeMatch()->getRouteName();
        if ($route_name == 'entity.node.canonical' || $route_name == 'entity.node.latest_version') {
            $node = \Drupal::routeMatch()->getParameter('node');
        } elseif ($route_name == 'entity.node.preview') {
            $node = \Drupal::routeMatch()->getParameter('node_preview');
        }

        if (is_object($node)) {
            $config = $this->getConfiguration();
            $field = $config['nc_same_tag_field'];
            $types = $config['nc_same_tag_types'];
            $sort = $config['nc_same_tag_sort'];
            $sortOrder = $config['nc_same_tag_sortOrder'];
            $display = $config['nc_same_tag_display'];

            //Types
            $tabTypes = [];
            foreach($types as $key => $value){
                if(is_string($value)){
                    $tabTypes[] = $key;
                }
            }

            if(!empty($tabTypes)){
                if ($node->hasField($field) && !empty($node->get($field)->getValue())) {
                    $tabTags = [];
                    foreach ($node->get($field)->getValue() as $tag) {
                        $tabTags[] = $tag['target_id'];
                    }

                    $query = \Drupal::entityQuery('node')
                        ->condition('type', $tabTypes, 'IN')
                        ->condition($field, $tabTags, 'IN')
                        ->condition('status', 1)
                        ->condition('nid', $node->id(), '!=');

                    $nids = $query->sort($sort, $sortOrder)
                        ->sort('created', 'DESC')
                        ->execute();

                    if (!empty($nids)) {
                        foreach ($nids as $nid) {
                            $nodeContent = Node::load($nid);
                            $tabTagsNode = $nodeContent->get($field)->getValue();
                            $countTags = 0;
                            if (!empty($tabTagsNode)) {
                                foreach ($tabTagsNode as $tabTagNode) {
                                    if (in_array($tabTagNode['target_id'], $tabTags)) {
                                        $countTags++;
                                    }
                                }
                            }
                            $tabNodes[$countTags][$nid] = $nodeContent;
                        }
                        arsort($tabNodes);

                        $result = [];
                        if (!empty($tabNodes)) {
                            foreach ($tabNodes as $key => $tabNode) {
                                $result = array_merge($result, $tabNode);
                            }

                            for ($i = 0; $i < 4; $i++) {
                                if (!empty($result[$i])) {
                                    $build = \Drupal::entityTypeManager()->getViewBuilder('node')->view($result[$i], $display);
                                    if (!empty($build)) {
                                        $contents[] = \Drupal::service('renderer')->render($build);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if(!empty($contents)){
            $build = [
                '#theme' => 'nc_same_tag',
                '#data' => $contents,
                '#cache' => [
                    'contexts' => ['url.query_args'],
                ],
            ];
        }else{
            $build = [];
        }

        return $build;
    }
}
