<?php
namespace Drupal\nc_links\Plugin\Block;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Links' Block.
 *
 * @Block(
 *   id = "nc_links",
 *   admin_label = @Translation("Links - Block"),
 * )
 */

class LinksBlock extends BlockBase implements ContainerFactoryPluginInterface {
	/**
	 * The current route match.
	 *
	 * @var \Drupal\Core\Routing\RouteMatchInterface
	 */
	protected $routeMatch;

	/**
	 * Constructs a new BookNavigationBlock instance.
	 *
	 * @param array $configuration
	 *   A configuration array containing information about the plugin instance.
	 * @param string $plugin_id
	 *   The plugin_id for the plugin instance.
	 * @param mixed $plugin_definition
	 *   The plugin implementation definition.
	 * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
	 *   The current route match.
	 */
	public function __construct(array $configuration, $plugin_id, $plugin_definition, RouteMatchInterface $route_match) {
		parent::__construct($configuration, $plugin_id, $plugin_definition);

		$this->routeMatch = $route_match;
	}

	/**
	 * {@inheritdoc}
	 */
	public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
		return new static(
			$configuration,
			$plugin_id,
			$plugin_definition,
			$container->get('current_route_match')
		);
	}

	/**
	 * {@inheritdoc}
	 */
	public function blockForm($form, FormStateInterface $form_state) {
		$form = parent::blockForm($form, $form_state);
		$config = $this->getConfiguration();

		$form['nc_links'] = [
			'#type' => 'fieldset',
			'#title' => "Configuration",
			'#tree' => TRUE,

			'field' => [
				'#type' => 'textfield',
				'#title' => $this->t('Field file name'),
				'#default_value' => isset($config['nc_links_field']) ? $config['nc_links_field'] : null,
			],
		];
		return $form;
	}

	/**
	 * {@inheritdoc}
	 */
	public function blockSubmit($form, FormStateInterface $form_state) {
		parent::blockSubmit($form, $form_state);
		$values = $form_state->getValues();

		$this->configuration['nc_links_field'] = $values['nc_links']['field'];
	}

	/**
	 * {@inheritdoc}
	 */
	public function build() {
		/** @var \Drupal\node\NodeInterface $node */
		$node = $this->routeMatch->getParameter('node');

		if (!($node instanceof NodeInterface)) {
			return [];
		}

		$data = [];

		$config = $this->getConfiguration();
		$field = $config['nc_links_field'];

		if($node->hasField($field) && count($node->get($field)->getValue()) > 0){
			foreach ($node->get($field)->getValue() as $link){
				$target = "_blank";
				$url = Url::fromUri($link['uri'])->toString();
				if(strpos($link['uri'], 'entity:node') !== false){ //Lien interne - via autocompletion
					$target = "";
				}

				$data[] = [
					'title' => !empty($link['title']) ? $link['title'] : $url,
					'url' => $url,
					'target' => $target,
				];
			}
		}

		if(count($data) > 0){
			return [
				'#theme' => 'nc_links_list',
				'#data' => $data,
			];
		}

		return [];
	}

	/**
	 * {@inheritdoc}
	 */
	public function getCacheTags() {
		// With this when your node change your block will rebuild.
		if ($node = \Drupal::routeMatch()->getParameter('node')) {
			// If there is node add its cachetag.
			return Cache::mergeTags(parent::getCacheTags(), ['node:' . $node->id()]);
		}
		else {
			// Return default tags instead.
			return parent::getCacheTags();
		}
	}

	public function getCacheContexts() {
		// Every new route this block will rebuild.
		return Cache::mergeContexts(parent::getCacheContexts(), ['route']);
	}
}
