var $activeCanvas, $currentParent;
(function (Drupal, debounce, $) {
  'use strict';
  Drupal.NcEditor = [];
  $.fn.NcEditor = function (format) {
    var $textarea = $(this);
    var editorIndex = Drupal.NcEditor.length;
    var NcEditor = {
      textarea: null,
      storage: [],
      settings: null,
      currentParent: null,
      ele: {
        document: $(document)
      },
      init: function ($textarea) {
        this.settings = format.editorSettings;
        this.ele.textarea = $textarea;
        try {
          this.storage = (this.ele.textarea.attr('data-editor-value-original') !== '') ? JSON.parse(this.ele.textarea.attr('data-editor-value-original')) : [];
        }
        catch (e) {
          this.storage = [];
        }
        this.cacheDOM();
        this.bindEvents();
        this.ele.textarea.after(this.ele.canvas);
        this.renderComponents(this.storage, this.ele.canvas);
        this.ele.canvas.find('.canvas-footer').append(this.ele.addRowButton);
        this.ele.canvas.find('.canvas-footer').append(this.ele.addCodeButton);
        this.ele.canvas.attr("data-index", editorIndex);
        this.ele.canvas.attr("data-textarea", "#" + this.ele.textarea.attr("id"));
      },
      cacheDOM: function () {
        var $this = this;
        this.ele.canvas = $("<div class=\"nce-canvas component\"><div class=\"canvas-body\"></div><div class=\"canvas-footer\"></div></div>");
        this.ele.addRowButton = $("<button type=\"button\" title=\"Ajouter une ligne\" class=\"btn btn-circle btn-primary btn-sm\"><i class=\"fa-regular fa-plus\" aria-hidden=\"true\"></i></button>");
        this.ele.addCodeButton = $("<button type=\"button\" title=\"Ajouter du CSS/Js\" class=\"btn btn-circle btn-primary btn-sm cssjs config-component\"><i class=\"fa-regular fa-code\" aria-hidden=\"true\"></i></button>");
        var lastItem = this.storage[this.storage.length - 1];
        var cssJsSettings = this.settings.buttons.code[0];
        if (this.storage.length > 0 && lastItem.type === 'code') {
          lastItem.editorIndex = editorIndex;
          cssJsSettings = lastItem;
        }
        this.ele.addCodeButton.attr('data-settings', JSON.stringify(cssJsSettings));
        this.ele.addButton = $("<button type=\"button\" title=\"Ajouter un composant\" class=\"btn btn-primary float-right btn-sm\"><i class=\"fa-regular fa-plus\" aria-hidden=\"true\"></i></button>");
        this.ele.componentButton = $("<button type=\"button\" class=\"btn btn-light add-component\"><i class=\"fa-regular fa-plus\" aria-hidden=\"true\"></i></button");
        this.ele.cloneButton = $("<button type=\"button\" class=\"btn btn-sm btn-light clone-component float-right\"><i class=\"fa-regular fa-clone\" aria-hidden=\"true\"></i></button");
        this.ele.deleteButton = $("<button type=\"button\" class=\"btn btn-sm btn-danger delete-component float-right\"><i class=\"fa-regular fa-trash\" aria-hidden=\"true\"></i></button");
        this.ele.configButton = $("<button type=\"button\" class=\"btn btn-sm btn-info config-component float-right\"><i class=\"fa-regular fa-cog\" aria-hidden=\"true\"></i></button");
        this.ele.minMaxButton = $("<button type=\"button\" class=\"btn btn-sm btn-light minmax-component float-right\"><i class=\"fa-regular fa-window-minimize\" aria-hidden=\"true\"></i></button");
        this.ele.components = $("<div class=\"components\">");
        this.ele.panel = $("<div class=\"component\"><div class=\"panel panel-default\">\n\
                                        <div class=\"panel-heading clearfix\"><h3 class=\"panel-title float-left\"></h3></div>\n\
                                        <div class=\"panel-body\"></div>\n\
                                        </div></div>");
        this.ele.row = $('<div id="nce-rows" class="components rows"></div>');
        this.ele.column = $('<div id="nce-columns" class="components columns"></div>');
        this.ele.widget = $('<div id="nce-widgets" class="components widgets"></div>');
        $.each(this.settings.buttons.row, function (i, item) {
          var button = $this.ele.componentButton.clone();
          item.editorIndex = editorIndex;
          button.attr('data-settings', JSON.stringify(item));
          button.html('<span class="icon ' + item.iconClass + '"></span><span class="label">' + item.name + '</span>');
          $this.ele.row.append(button);
        });
        $.each(this.settings.buttons.column, function (i, item) {
          var button = $this.ele.componentButton.clone();
          item.editorIndex = editorIndex;
          button.attr('data-settings', JSON.stringify(item));
          button.html('<span class="icon ' + item.iconClass + '"></span><span class="label">' + item.name + '</span>');
          $this.ele.column.append(button);
        });
        $.each(this.settings.buttons.widget, function (i, item) {
          var button = $this.ele.componentButton.clone();
          item.editorIndex = editorIndex;
          button.attr('data-settings', JSON.stringify(item));
          button.html('<span class="icon ' + item.iconClass + '"></span><span class="label">' + item.name + '</span>');
          $this.ele.widget.append(button);
        });
        this.renderComponentButtons();
      },
      bindEvents: function () {
        var $this = this;
        this.ele.document.once("addComponent").on("click", ".add-component", function (e) {
          $(e.currentTarget).parents('.popover').popover('hide');
          var settings = $(e.currentTarget).data("settings");
          $activeCanvas = $(e.currentTarget).closest(".nce-canvas");
          if (settings.type === 'column') {
            var grid = settings.grid.split('_');
            var colSettings = settings;
            colSettings.adminClass += 'col-xl-' + grid[1];
            colSettings.name = 'Column ' + grid[1];
            colSettings.content = {
              device: [{
                lg: grid[1],
                md: grid[1],
                sm: grid[1],
                xs: grid[0]
              }]
            };
            $this.renderComponents([colSettings], $(e.currentTarget).closest(".component"));
          }
          else {
            $this.renderComponents([settings], $(e.currentTarget).closest(".component"));
            if (settings.type === 'widget') {
              $(e.currentTarget).closest(".component").find(".panel-body .config-component:last()").trigger("click");
            }
          }
          $this.updateStorage($this.ele.canvas.find("> .canvas-body > .component"), []);
        });
        this.ele.document.once("deleteComponent").on("click", ".delete-component", this.deleteComponent.bind(this));
        this.ele.document.once("cloneComponent").on("click", ".clone-component", this.cloneComponent.bind(this));
        this.ele.document.once("configComponent").on('click', ".config-component", this.openComponentForm.bind(this));
        this.ele.document.once("minMaxComponent").on('click', ".minmax-component", this.toggleMinMax.bind(this));
      },
      toggleMinMax: function (e) {
        console.log($(e.currentTarget).closest(".panel").find(".panel-body"));
        $(e.currentTarget).closest(".panel").find(".panel-body").toggleClass('d-none');
        $(e.currentTarget).find(".fa").toggleClass("fa-window-minimize").toggleClass("fa-window-maximize");
      },
      renderComponentButtons: function () {
        var $this = this;
        this.ele.addRowButton.popover({
          html: true,
          content: $this.ele['row'],
          placement: 'top',
          trigger: 'click',
          delay: {"hide": 100},
          container: this.ele.canvas.find('.canvas-footer')
        });
      },
      renderComponents: function (items, $target) {
        var $this = this;
        $(items).each(function (i, item) {
          if (item.type === 'code') {
            return;
          }
          var $panel = $this.ele.panel.clone();
          var $cloneButton = $this.ele.cloneButton.clone();
          var $deleteButton = $this.ele.deleteButton.clone();
          var $configButton = $this.ele.configButton.clone();
          var $minMaxButton = $this.ele.minMaxButton.clone();
          var componentTitle = item.attributes.title ? item.attributes.title : item.name;
          $panel.attr('data-settings', JSON.stringify(item));
          $panel.find("> .panel > .panel-heading > .panel-title").text(componentTitle);
          $panel.find("> .panel > .panel-heading").append($minMaxButton);
          $panel.find("> .panel > .panel-heading").append($deleteButton);
          $panel.find("> .panel > .panel-heading").append($cloneButton);
          $panel.find("> .panel > .panel-heading").append($configButton);
          if (item.type === 'widget') {
            var contentWidget = ncEditorContentEditor(item.id, item);
            $panel.find('.panel-body').html(contentWidget);
          }
          item.adminClass = item.adminClass ? item.adminClass + ' nce-' + item.type : 'nce-' + item.type;
          $panel.addClass(item.adminClass);
          if (item.accepts && item.accepts.length) {
            var $addButton = $this.ele.addButton.clone();
            $addButton.popover({
              html: true,
              content: $this.ele[item.accepts],
              placement: 'bottom',
              trigger: 'click',
              delay: {"hide": 100},
              container: $panel.find("> .panel > .panel-heading")
            });

            if (item.type === 'column') {
              if (item.content.device) {
                $.each(item.content.device[0], function (key, val) {
                  $panel.addClass('col-' + key + '-' + val);
                });
              }
              else {
                $.each(item.content, function (key, val) {
                  var classStr = key.replace(/_/g, "-");
                  $panel.addClass(classStr + "-" + val);
                });
              }
              if (item.attributes.class) {
                $panel.addClass(item.attributes.class);
              }
            }
            $panel.find("> .panel > .panel-heading").append($addButton);
          }
          else {
            $panel.find(".panel-footehoverr").remove();
          }
          if (item.children && item.children.length) {
            $this.renderComponents(item.children, $panel);
          }
          if (item.type !== 'row') {
            $target.find("> .panel > .panel-body").append($panel);
            $target.find(".col").parent().addClass("row");
          }
          else {
            $target.find(".canvas-body").append($panel);
            $target.find('.nce-row > .panel > .panel-body').addClass('row');
          }
        });

        $('.nce-row > .panel > .panel-body, .canvas-body').sortable({
          cursor: 'move',
          update: function (event, ui) {
            $this.updateStorage($this.ele.canvas.find("> .canvas-body > .component"), []);
          }
        });
        $('.nce-column > .panel > .panel-body').sortable({
          cursor: 'move',
          connectWith: '.nce-column > .panel > .panel-body',
          update: function (event, ui) {
            $this.updateStorage($this.ele.canvas.find("> .canvas-body > .component"), []);
          }
        });
      },
      deleteComponent: function (e) {
        var $ele = $(e.target).closest(".component");
        $activeCanvas = $ele.closest(".nce-canvas");
        $ele.remove();
        this.updateStorage($activeCanvas.find("> .canvas-body > .component"), []);
      },
      cloneComponent: function (e) {
        var $ele = $(e.target).closest(".component");
        $activeCanvas = $ele.closest(".nce-canvas");
        var settings = JSON.parse($ele.attr("data-settings"));
        this.renderComponents([settings], $ele.parent().parent().parent());
        this.updateStorage($activeCanvas.find("> .canvas-body > .component"), []);
      },
      openComponentForm: function (e) {
        e.preventDefault();
        var $ele = ($(e.currentTarget).hasClass("cssjs")) ? $(e.currentTarget) : $(e.currentTarget).closest(".component");
        var settings = $ele.attr('data-settings');
        $activeCanvas = $ele.closest(".nce-canvas");
        $(".nc-editor-component-utility-form").remove();
        $currentParent = $ele;
        $(e.currentTarget).parent().append(this.settings.componentUtilityForm);
        this.ele.document
            .find('.' + this.settings.componentUtilityFormWrap + ' .nc-editor-settings')
            .val(settings);
        Drupal.behaviors.AJAX.attach($('.use-ajax-submit'), []);
        this.ele.document.find('.use-ajax-submit').once('useAjaxSubmit').trigger('click');

        var settingsWidget = JSON.parse(settings);
        if (settingsWidget.id === 'image' || settingsWidget.id === 'slider') {
          getAllPreview();
        }
      },
      updateStorage: function ($selector, storage) {
        var $this = this;
        $activeCanvas = $selector.closest(".nce-canvas");
        $selector.each(function (i) {
          var settings = JSON.parse($(this).attr('data-settings'));
          settings.children = [];
          if ($(this).find('>.panel>.panel-body>.component').length > 0) {
            settings.children = $this.updateStorage($(this).find('>.panel>.panel-body>.component'), []);
          }
          $(this).attr('data-settings', JSON.stringify(settings));
          storage.push(settings);
        });
        if (storage.length === 0 || storage[0].type === 'row') {
          storage.push(JSON.parse(this.ele.addCodeButton.attr('data-settings')));
        }
        $($activeCanvas.data("textarea")).val(JSON.stringify(storage)).trigger('change');
        $($activeCanvas.data("textarea")).attr('data-editor-value-original', JSON.stringify(storage));
        return storage;
      },
      destroy: function () {
        this.updateStorage($activeCanvas.find("> .canvas-body > .component"), []);
        this.ele.canvas.remove();
        $(".btn").unbind("click");
      },
      getColClasses: function (colClasses) {
        var classes = 'component';
        $.each(colClasses.device[0], function (i, val) {
          if (val !== '') {
            var className = 'col-' + i + '-' + val;
            classes += ' ' + className;
          }
        });

        $.each(colClasses.device[1], function (i, val) {
          if (val !== '') {
            var className = 'offset-' + i + '-' + val;
            classes += ' ' + className;
          }
        });
        return classes;
      }
    };
    NcEditor.init($textarea);
    return NcEditor;
  };

  /**
   * @namespace
   */
  Drupal.editors.nc_editor = {
    /**
     * Editor attach callback.
     *
     * @param {HTMLElement} element
     *   The element to attach the editor to.
     * @param {string} format
     *   The text format for the editor.
     *
     * @return {bool}
     *   Whether the call to `CKEDITOR.replace()` created an editor or not.
     */
    attach: function (element, format) {
      Drupal.NcEditor.push($(element).NcEditor(format));
    },
    /**
     * Editor detach callback.
     *
     * @param {HTMLElement} element
     *   The element to detach the editor from.
     * @param {string} format
     *   The text format used for the editor.
     * @param {string} trigger
     *   The event trigger for the detach.
     *
     * @return {bool}
     *   Whether the call to `CKEDITOR.dom.element.get(element).getEditor()`
     *   found an editor or not.
     */
    detach: function (element, format, trigger) {
      if (trigger !== 'serialize') {
        $activeCanvas = $(element).parent().find("> .nce-canvas");
        Drupal.NcEditor[$activeCanvas.data("index")].destroy();
      }
    },
    /**
     * Reacts on a change in the editor element.
     *
     * @param {HTMLElement} element
     *   The element where the change occured.
     * @param {function} callback
     *   Callback called with the value of the editor.
     *
     * @return {bool}
     *   Whether the call to `CKEDITOR.dom.element.get(element).getEditor()`
     *   found an editor or not.
     */
    onChange: function (element, callback) {

    }
  };

  $.fn.updateComponent = function (settings) {
    $(this).val(JSON.stringify(settings));
    settings = JSON.parse(settings);
    var editor = Drupal.NcEditor[settings.editorIndex];
    if (settings.type !== 'code') {
      var componentTitle = settings.attributes.title ? settings.attributes.title : settings.name;
      $currentParent.find("> .panel > .panel-heading > .panel-title").text(componentTitle);
      if (settings.type === 'widget') {
        var contentWidget = ncEditorContentEditor(settings.id, settings);
        $currentParent.find('.panel-body').html(contentWidget);
      }
      if (settings.type === 'column') {
        var colClasses = editor.getColClasses(settings.content);
        $currentParent.attr("class", colClasses);
        settings.adminClass = colClasses;
      }
      $currentParent.attr('data-settings', JSON.stringify(settings));
      editor.updateStorage($currentParent.closest(".canvas-body").find("> .component"), []);
      editor.storage.push(settings);
    }
    else {
      var storageLen = editor.storage.length;
      editor.ele.addCodeButton.attr('data-settings', JSON.stringify(settings));
      if (editor.storage.length > 0 && editor.storage[storageLen - 1].type == 'code') {
        editor.storage[storageLen - 1] = settings;
      }
      else {
        editor.storage[storageLen] = settings;
      }
      editor.updateStorage(editor.ele.canvas.find("> .canvas-body > .component"), []);
    }
  };


  function ncEditorContentEditor(id, item) {
    var content = '',
        show = '',
        is_expanded = '';

    switch (id) {
      case 'accordions':
        content = '<div id="accordion">';
        $.each(item.content.accordions, function (index, value) {
          show = '';
          is_expanded = 'false';
          if (value.accordion.is_collapsed === 1) {
            show = 'show';
            is_expanded = 'true';
          }
          content += '<div class="card">' +
              '<div class="card-header" id="heading-nce-accordion--' + index + '">' +
              '<a class="card-link" data-toggle="collapse" href="#nce-accordion--' + index + '" aria-expanded="' + is_expanded + '">' + value.accordion.title + '</a>' +
              '</div>' +
              '<div id="nce-accordion--' + index + '" class="collapse ' + show + '" data-parent="#accordion">' +
              '<div class="card-body">' + value.accordion.text.value + '</div>' +
              '</div>' +
              '</div>';
        });
        content += '</div>';
        break;

      case 'button':
        content = '';
        if (typeof item.content.url !== 'undefined') {
          var size = item.content.size,
              full = item.content.full,
              outline = item.content.outline;

          if (size !== '') {
            size = 'btn-' + size;
          }
          if (full !== '') {
            full = 'btn-' + full;
          }
          if (outline !== '') {
            outline = outline + '-';
          }
          content = '<div class="text-' + item.content.align + '">';
          content += '<a href="' + item.content.url + '" class="btn btn-' + outline + item.content.style + ' ' + size + ' ' + full + '" target="' + item.content.target + '">' + item.content.label + '</a>';
          content += '</div>';
        }
        break;

      case 'heading':
        content = '';
        if (typeof item.content.heading !== 'undefined') {
          content = '<' + item.content.type + ' class="text-' + item.content.align + '">' + item.content.heading + '</' + item.content.type + '>';
        }
        break;

      case 'image':
        content = '';
        if (item.content !== '') {
          content += '<figure class="figure">' +
              '<img src="' + item.content.image + '" class="img-fluid">' +
              '</figure>';
        }
        break;

      case 'slider':
        content = '<div class="row">';
        if (item.content !== '') {
          var sliders = item.content.sliders;
          $.each(sliders, function (index, value) {
            if (index < item.options.number) {
              $.ajax({
                url: '/nc-editor/ajax/image',
                type: 'GET',
                data: {
                  image: value.slider.image,
                  format: 'nc_editor_slider'
                },
                async: false,
                success: function (data) {
                  content += '<div class="col-xs-' + (12 / item.options.number) + ' col-sm-' + (12 / item.options.number) + ' col-lg-' + (12 / item.options.number) + ' col-xl-' + (12 / item.options.number) + '">' +
                      '<img src="' + data + '" class="img-fluid">' +
                      '</div>';
                },
              });
            }
          });
        }
        content += '</div>';
        break;

      case 'blockquote':
        content += '<div>';

        if (typeof item.content.description !== 'undefined') {
          content += '<div>' + item.content.description.value + '</div>';
        }

        content += '<div><b>' + item.content.legende + '</b></div>' +
            '</div>';
        break;

      case 'highlight':
        if (typeof item.content.highlight !== 'undefined') {
          content = '<div>' + item.content.highlight.value + '</div>';
        }
        break;

      case 'pourcentage':
        content += '<div class="text-center">';

        if (typeof item.content.pourcentage !== 'undefined') {
          content += '<div>' + item.content.pourcentage + '%</div>';
        }

        if (typeof item.content.texte !== 'undefined') {
          content += '<div>' + item.content.texte + '</div>';
        }

        content += '</div>';
        break;

      case 'webform':
        //item.content.webform
        content = '';
        if (typeof item.content.webform !== 'undefined') {
          $.ajax({
            url: '/nc-editor/ajax/webform',
            type: 'GET',
            data: {
              id: item.content.webform,
            },
            async: false,
            success: function (data) {
              content += data;
            },
          });
        }
        break;

      case 'wysiwyg':
        content = '';
        if (typeof item.content.wysiwyg !== 'undefined') {
          content = item.content.wysiwyg.value;
        }
        break;

      case 'formation':
        content = '';
        if (typeof item.content.categories !== 'undefined') {
          $.each(item.content.categories, function (index, value) {
            if (value !== 0) {
              $.ajax({
                url: '/nc-editor/ajax/formation',
                type: 'GET',
                data: {
                  id: value,
                },
                async: false,
                success: function (data) {
                  content += 'Catégorie de formations "'+ data +'"<br>';
                },
              });
            }
          });
        }
        break;

      case 'youtube':
        content = '<iframe width="100%" src="https://www.youtube.com/embed/' + item.content.embed_url + '?rel=0">';
        break;

      default:
        content = '<p><strong>Widget : </strong>' + item.name + '</p>';
        break;
    }

    return content;
  }

  function getAllPreview() {
    setTimeout(function () {
      $('.imce-url-input').each(function (index) {
        previewImage($(this));
      });
    }, 1000);
  }

  function previewImage(element) {
    var elementParent = element.parent().parent();
    if (element.val() !== '') {
      elementParent.find('.imce_preview').html('<img src="' + element.val() + '" width="100px">');
    }
  }

  if ($('.imce-url-input').length > 0) {
    getAllPreview();
  }

  $('body').on('change', '.imce-url-input', function () {
    getAllPreview();
  });

  $('body').on('click', '#sliders-wrapper summary', function () {
    getAllPreview();
  });
})(Drupal, Drupal.debounce, jQuery);
