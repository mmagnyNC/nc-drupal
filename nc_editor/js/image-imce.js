(function(Drupal, $) {
  $('.imce-url-input').on('change', function(){
    var elementParent = $(this).parent().parent();
    elementParent.find('.imce_preview').html('<img src="'+$(this).val()+'" width="100px">');
  });
})(Drupal, jQuery);
