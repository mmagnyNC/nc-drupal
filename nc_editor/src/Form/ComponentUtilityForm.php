<?php

/**
 * @file
 * Contains \Drupal\nc_editor\Form\ComponentUtilityForm.
 */

namespace Drupal\nc_editor\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class ComponentUtilityForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'nc_editor_component_utility_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#action'] = \Drupal\Core\Url::fromRoute('component.form', ['method' => 'nojs'])->toString();
    $form['nc_editor_settings'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'class' => [
          'nc-editor-settings'
        ]
      ]
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => t('Submit'),
      '#attributes' => [
        'class' => [
          'use-ajax-submit'
        ],
        'style' => 'display:none'
      ]
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
