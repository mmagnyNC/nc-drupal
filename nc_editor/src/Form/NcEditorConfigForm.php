<?php

namespace Drupal\nc_editor\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\nc_editor\NcEditorConfig;

/**
 * Configure Nc Editor settings.
 */
class NcEditorConfigForm extends ConfigFormBase
{
	/**
	 * {@inheritdoc}
	 */
	public function getFormId()
	{
		return 'nc_editor_config';
	}

	/**
	 * {@inheritdoc}
	 */
	protected function getEditableConfigNames()
	{
		return [
			'nc_editor.config',
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function buildForm(array $form, FormStateInterface $form_state)
	{
		$config = $this->config('nc_editor.config');
		$form['nce_config'] = [
			'#type' => 'vertical_tabs',
		];

		$frontend_config = $config->get('frontend');
		$form['frontend'] = [
			'#type' => 'details',
			'#title' => t('Frontend Configuration'),
			'#group' => 'nce_config',
			'#tree' => true
		];
		$form['frontend']['framework'] = [
			'#type' => 'select',
			'#title' => $this->t('Framework'),
			'#options' => [
				'12' => "12 " . t('columns'),
			],
			'#default_value' => (!empty($frontend_config['framework'])) ? '12' : $frontend_config['framework'],
		];

		//Components activés
		$components_config = $config->get('components');
		$form['components'] = [
			'#type' => 'details',
			'#title' => t('Components'),
			'#description' => t('Select the components to activate in the editor'),
			'#group' => 'nce_config',
			'#tree' => true
		];

		$nceConfig = new NcEditorConfig();
		$components = $nceConfig->getComponents();
		foreach($components['widget'] as $widget){
			$form['components'][$widget['id']] = [
				'#type' => 'checkbox',
				'#title' => "<i class='".$widget['iconClass']."'></i>".$widget['name'],
				'#default_value' => (isset($components_config[$widget['id']])) ? $components_config[$widget['id']] : 0,
			];
		}

		return parent::buildForm($form, $form_state);
	}

	/**
	 * {@inheritdoc}
	 */
	public function submitForm(array &$form, FormStateInterface $form_state)
	{
		$this->configFactory->getEditable('nc_editor.config')
			->set('frontend', $form_state->getValue('frontend'))
			->set('components', $form_state->getValue('components'))
			->save();
		parent::submitForm($form, $form_state);
	}
}
