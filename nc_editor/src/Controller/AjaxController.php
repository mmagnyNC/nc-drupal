<?php

namespace Drupal\nc_editor\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\image\Entity\ImageStyle;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\HttpFoundation\JsonResponse;

class AjaxController extends ControllerBase {

  public function getImage($item = NULL) {
    $image = \Drupal::request()->query->get('image');
    $format = \Drupal::request()->query->get('format');

    if (!empty($image) && !empty($format)) {
      $imageUri = str_replace('/sites/default/files', 'public:/', $image);
      $image = ImageStyle::load($format)->buildUrl($imageUri, TRUE);
    }

    return new JsonResponse($image);
  }

  public function getWebform() {
    $id = \Drupal::request()->query->get('id');

    $webform = \Drupal::entityTypeManager()->getStorage('webform')->load($id);

    return new JsonResponse('Formulaire "' . $webform->get('title') . '"');
  }

  public function getFormation() {
    $id = \Drupal::request()->query->get('id');
    $term = Term::load($id);
    return new JsonResponse($term->getName());
  }
}
