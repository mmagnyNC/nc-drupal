<?php

/**
 * @file
 * Provides Drupal\nc_editor\NcEditorInterface
 */

namespace Drupal\nc_editor;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Form\FormStateInterface;

interface NcEditorInterface extends PluginInspectionInterface {
  
  /**
   * Return buttons.
   *
   * @return array
   */
  public function getButtons();
  
  /**
   * Return settings.
   *
   * @return array
   */
  public function submitForm(array &$form, FormStateInterface $form_state, array $settings);
}
