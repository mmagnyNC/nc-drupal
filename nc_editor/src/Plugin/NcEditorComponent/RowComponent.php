<?php
/**
 * @file
 * Contains \Drupal\nc_editor\Plugin\NcEditorComponent\RowComponent.
 */
namespace Drupal\nc_editor\Plugin\NcEditorComponent;
use Drupal\nc_editor\NcEditorBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'row' Nc Editor component.
 *
 * @NcEditorComponent(
 *   id = "row",
 *   type = "row",
 *   name = @Translation("Row"),
 *   iconClass = "fa-regular fa-bars",
 *   accepts = "column",
 * )
 */
class RowComponent extends NcEditorBase {
  
  public function buildForm(array &$form, FormStateInterface $form_state, array $settings){

  }
  
  public function render($settings){
    $build = parent::getBuild($settings);
    $build['#theme'] = 'nce_component';
    $build['#attributes']['class'][] = 'row';
    return $build;
  }
}
