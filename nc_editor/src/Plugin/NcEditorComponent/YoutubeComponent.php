<?php
/**
 * @file
 * Contains \Drupal\nc_editor\Plugin\NcEditorComponent\YoutubeComponent.
 */
namespace Drupal\nc_editor\Plugin\NcEditorComponent;
use Drupal\nc_editor\NcEditorBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'youtube' Nc Editor component.
 *
 * @NcEditorComponent(
 *   id = "youtube",
 *   type = "widget",
 *   name = "Youtube",
 *   iconClass = "fa-brands fa-youtube",
 * )
 */
class YoutubeComponent extends NcEditorBase {
	public function buildForm(array &$form, FormStateInterface $form_state, array $settings){
		$content = $settings['content'];
		$form['content']['embed_url'] = [
			'#type' => 'textfield',
			'#required' => true,
			'#title' => "Code de la vidéo Youtube",
			'#default_value' => isset($content['embed_url']) ? $content['embed_url'] : '',
		];
		$options = \Drupal::service('plugin.manager.nc_editor')->getFrontendConfig('aspect_ratio');
		$form['content']['aspect_ratio'] = [
			'#type' => 'select',
			'#required' => true,
			'#title' => "Ratio",
			'#options' => $options,
			'#default_value' => isset($content['aspect_ratio']) ? $content['aspect_ratio'] : '',
		];
	}
	public function render($settings){
		$build = parent::getBuild($settings);
		$build['#theme'] = 'nce_embeded_video';
		$build['#content'] = $settings['content'];
		return $build;
	}
}
