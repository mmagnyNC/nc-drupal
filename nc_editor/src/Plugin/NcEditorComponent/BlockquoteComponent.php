<?php

/**
 * @file
 * Contains \Drupal\nc_editor\Plugin\NcEditorComponent\BlockquoteComponent.
 */

namespace Drupal\nc_editor\Plugin\NcEditorComponent;

use Drupal\image\Entity\ImageStyle;
use Drupal\nc_editor\NcEditorBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'blockquote box' Nc Editor component.
 *
 * @NcEditorComponent(
 *   id = "blockquote",
 *   type = "widget",
 *   name = "Citation",
 *   iconClass = "fa-regular fa-block-quote",
 * )
 */
class BlockquoteComponent extends NcEditorBase
{

	public function buildForm(array &$form, FormStateInterface $form_state, array $settings)
	{
		$content = $settings['content'];

		$form['content']['description'] = [
			'#type' => 'text_format',
			'#required' => true,
			'#title' => "Contenu",
			'#default_value' => isset($content['description']['value']) ? $content['description']['value'] : '',
			'#format' => isset($content['description']['format']) ? $content['description']['format'] : 'full_html',
			'#allowed_formats' => unserialize(ALLOWED_TEXT_FORMATS),
			'#prefix' => '<div id="wysiwyg">',
			'#suffix' => '</div>',
		];

		$form['content']['legende'] = [
			'#title' => "Légende",
			'#type' => 'textfield',
			'#default_value' => isset($content['legende']) ? $content['legende'] : '',
		];
	}

	public function submitForm(array &$form, FormStateInterface $form_state, array $settings) {
		$settings = parent::submitForm($form, $form_state, $settings);
		$user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
		$text = $form_state->getValue('content');
		$uuids = _editor_parse_file_uuids($text['description']['value']);
		_editor_record_file_usage($uuids, $user);
		return $settings;
	}

	public function render($settings)
	{
		$build = parent::getBuild($settings);

		$content = $settings['content'];
		$build['#theme'] = 'nce_blockquote';

		//Contenu
		$build['#content']['description'] = $content['description']['value'];

		//Légende
		$build['#content']['legende'] = $content['legende'];

		return $build;
	}

}
