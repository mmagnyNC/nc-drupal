<?php

/**
 * @file
 * Contains \Drupal\nc_editor\Plugin\NcEditorComponent\HtmlComponent.
 */

namespace Drupal\nc_editor\Plugin\NcEditorComponent;

use Drupal\image\Entity\ImageStyle;
use Drupal\nc_editor\NcEditorBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'html box' Nc Editor component.
 *
 * @NcEditorComponent(
 *   id = "html",
 *   type = "widget",
 *   name = "Html",
 *   iconClass = "fa-regular fa-code",
 * )
 */
class HtmlComponent extends NcEditorBase {

  public function buildForm(array &$form, FormStateInterface $form_state, array $settings) {
    $content = $settings['content'];

    $form['content']['html'] = [
      '#type' => 'textarea',
      '#required' => TRUE,
      '#title' => t('Html'),
      '#default_value' => isset($content['html']) ? $content['html'] : '',
    ];

  }

  public function render($settings) {
    $build = parent::getBuild($settings);

    $content = $settings['content'];
    $build['#theme'] = 'nce_html';

    //Html
    $build['#content']['html'] = $content['html'];

    return $build;
  }

}
