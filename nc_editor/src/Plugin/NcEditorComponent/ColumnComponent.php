<?php
/**
 * @file
 * Contains \Drupal\nc_editor\Plugin\NcEditorComponent\ColumnComponent.
 */
namespace Drupal\nc_editor\Plugin\NcEditorComponent;
use Drupal\nc_editor\NcEditorBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'column' NC Editor component.
 *
 * @NcEditorComponent(
 *   id = "column",
 *   type = "column",
 *   name = @Translation("Column"),
 *   iconClass = "fa-regular fa-th",
 *   accepts = "widget",
 * )
 */
class ColumnComponent extends NcEditorBase {
	public function getButtons() {
		$config = \Drupal::config('nc_editor.config');
		$numberCols = (!empty($config->get('frontend')['framework'])) ? 12 : $config->get('frontend')['framework'];

		$tabCols = [];
		for($i = 1; $i <= $numberCols; $i++){
			$tabCols[] = $i;
		}
		rsort($tabCols);

		$buttons = parent::getButtons();
		$cols = [];
		foreach($tabCols as $i => $id){
			$cols[$i] = $buttons;
			$cols[$i]['name'] .= " " . $id;
			$cols[$i]['grid'] = $numberCols.'_'.$id;
		}
		return $cols;
	}

	public function buildForm(array &$form, FormStateInterface $form_state, array $settings){
		$config = \Drupal::config('nc_editor.config');
		$numberCols = $config->get('columns')['number'];

		$content = $settings['content'];
		$col_size = \Drupal::service('plugin.manager.nc_editor')->getFrontendConfig('col_size');
		$col_offset_size = \Drupal::service('plugin.manager.nc_editor')->getFrontendConfig('col_offset_size');
		$devices = \Drupal::service('plugin.manager.nc_editor')->getFrontendConfig('device');
		// Column size element
		$form['content']['device'] = [
			'#type' => 'table',
			'#title' => 'Column size',
			'#header' => $devices,
		];
		foreach($devices as $key => $title){
			$form['content']['device'][0][$key] = [
				'#type' => 'select',
				'#title' => t('Size'),
				'#options' => $col_size,
				'#default_value' => isset($content['device'][0][$key]) ? $content['device'][0][$key] : $numberCols,
			];
			if($key == 'xl'){
				$form['content']['device'][0][$key]['#default_value'] = $form['content']['device'][0]['lg']['#default_value'];
			}
		}
		foreach($devices as $key => $title){
			$form['content']['device'][1][$key] = [
				'#type' => 'select',
				'#title' => t('Offset'),
				'#options' => $col_offset_size,
				'#default_value' => isset($content['device'][1][$key]) ? $content['device'][1][$key] : '0',
			];
		}
	}

	public function render($settings){
		$build = parent::getBuild($settings);
		$devices = \Drupal::service('plugin.manager.nc_editor')->getFrontendConfig('device');
		$device_offset = \Drupal::service('plugin.manager.nc_editor')->getFrontendConfig('device_offset');
		$content = $settings['content'];
		foreach($devices as $item => $label){
			if(!empty($content['device'][0][$item])){
				$build['#attributes']['class'][] = 'col-'. $item .'-'.$content['device'][0][$item];
			}
			if(!empty($content['device'][1][$item])){
				$build['#attributes']['class'][] = $device_offset[$item].$content['device'][1][$item];
			}
		}

		$build['#theme'] = 'nce_component';
		return $build;
	}
}
