<?php

/**
 * @file
 * Contains \Drupal\nc_editor\Plugin\NcEditorComponent\ButtonComponent.
 */

namespace Drupal\nc_editor\Plugin\NcEditorComponent;

use Drupal\nc_editor\NcEditorBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides a 'button' Nc Editor component.
 *
 * @NcEditorComponent(
 *   id = "button",
 *   type = "widget",
 *   name = "Bouton",
 *   iconClass = "fa-regular fa-link",
 * )
 */
class ButtonComponent extends NcEditorBase
{

	public function buildForm(array &$form, FormStateInterface $form_state, array $settings)
	{
		$content = $settings['content'];
		$form['content']['label'] = [
			'#title' => t('Label'),
			'#type' => 'textfield',
			'#required' => true,
			'#default_value' => isset($content['label']) ? $content['label'] : '',
		];

		$form['content']['url'] = [
			'#title' => t('URL'),
			'#type' => 'url',
			'#required' => true,
			'#default_value' => (isset($content['url'])) ? static::getUriAsDisplayableString($content['url']) : NULL,
			'#element_validate' => [[get_called_class(), 'validateUriElement']],
			'#description' => 'Pour déposer un fichier, ouvrez le "<u><a href="/imce" target="_blank" title="Navigateur de fichiers">Navigateur de fichiers</a></u>"'
		];

		$form['content']['target'] = [
			'#title' => t('Target'),
			'#type' => 'select',
			'#empty_option' => t(' - Select - '),
			'#options' => [
				'_blank' => 'Lien externe (_blank)',
			],
			'#default_value' => isset($content['target']) ? $content['target'] : '',
		];

		$form['content']['style'] = [
			'#type' => 'select',
			'#title' => t('Style'),
			'#options' => [
				'primary' => "Vert",
				'secondary' => "Blanc",
			],
			'#default_value' => isset($content['style']) ? $content['style'] : 'primary',
		];

		$form['content']['align'] = [
			'#type' => 'select',
			'#title' => t('Alignment'),
			'#options' => [
				'left' => t('Left'),
				'center' => t('Center'),
				'right' => t('Right'),
			],
			'#default_value' => isset($content['align']) ? $content['align'] : 'left',
		];
	}

	public function render($settings)
	{
		$build = parent::getBuild($settings);
		$build['#theme'] = 'nce_button';
		$content = $settings['content'];

		if(!empty($content['url'])){
			if(strpos($content['url'], 'internal:/') === 0){
				$content['url'] = str_replace('internal:/', '/', $content['url']);
			}
			$build['#content'] = $content;

			if (!empty($content['target'])) {
				$build['#attributes']['target'] = $content['target'];
			}

			$build['#attributes']['class'] = [
				'btn',
				'btn-' . $content['style'],
			];
		}
		return $build;
	}

}
