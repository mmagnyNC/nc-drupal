<?php

/**
 * @file
 * Contains \Drupal\nc_editor\Plugin\NcEditorComponent\FormationsComponent.
 */

namespace Drupal\nc_editor\Plugin\NcEditorComponent;

use Drupal\nc_editor\NcEditorBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;

/**
 * Provides a 'formation' Nc Editor component.
 *
 * @NcEditorComponent(
 *   id = "formation",
 *   type = "widget",
 *   name = "Formations",
 *   iconClass = "fa-regular fa-list-tree",
 * )
 */
class FormationsComponent extends NcEditorBase
{

	public function buildForm(array &$form, FormStateInterface $form_state, array $settings)
	{
		$content = $settings['content'];

    $categories = [];
    $query = \Drupal::entityQuery('taxonomy_term')
      ->condition('vid', 'categories_formation')
      ->condition('status', 1);

    $terms = $query->sort('weight', 'ASC')
      ->execute();

    if( !empty($terms) ){
      foreach ( $terms as $term ) {
        $termObject = Term::load($term);
        $categories[$term] = $termObject->getName();
      }
    }

		$form['content']['categories'] = [
      '#type' => 'checkboxes',
      '#options' => $categories,
      '#title' => "Catégories de formation",
      '#required' => true,
      '#default_value' => isset($content['categories']) ? $content['categories'] : '',
		];
	}

	public function render($settings)
	{
		$build = parent::getBuild($settings);
		$build['#theme'] = 'nce_formations';
    $content = $settings['content'];
    $data = [];

    // Requête de récupération des contenus

    foreach ($content['categories'] as $category){
      if(!empty($category)){
        $term = Term::load($category);
        if(!empty($term)){
          $data['categories'][$category]['title'] = $term->getName();

          $query = \Drupal::entityQuery('node')
            ->condition('type', 'formation')
            ->condition('status', 1)
            ->condition('field_categorie', $category);

          $nids = $query->sort('title', 'ASC')
            ->execute();

          // Traitement des résultats

          if ( !empty($nids) ) {
            foreach ($nids as $nid) {

              // Chargement d'un résultat

              $node = Node::load($nid);

              if (!empty($node) && is_object($node)) {

                // Récupération de son mode d'affichage

                $buildNode = \Drupal::entityTypeManager()
                  ->getViewBuilder('node')
                  ->view($node, 'teaser');

                if (!empty($buildNode)) {

                  // Rendu d'affichage

                  $data['categories'][$category]['formations'][] = \Drupal::service('renderer')->render($buildNode);
                }
              }
            }
          }
        }
      }
    }

    $build['#content'] = $data;
		return $build;
	}
}
