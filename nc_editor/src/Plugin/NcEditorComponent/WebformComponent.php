<?php

/**
 * @file
 * Contains \Drupal\nc_editor\Plugin\NcEditorComponent\WebformComponent.
 */

namespace Drupal\nc_editor\Plugin\NcEditorComponent;

use Drupal\nc_editor\NcEditorBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'webform' Nc Editor component.
 *
 * @NcEditorComponent(
 *   id = "webform",
 *   type = "widget",
 *   name = "Formulaire",
 *   iconClass = "fa-brands fa-wpforms",
 * )
 */
class WebformComponent extends NcEditorBase {

	public function buildForm(array &$form, FormStateInterface $form_state, array $settings) {
		$content = $settings['content'];
		$form['content']['webform'] = [
			'#type' => 'entity_autocomplete',
			'#target_type' => 'webform',
			'#required' => true,
			'#title' => t('Webform Id'),
			'#process_default_value' => false,
			'#default_value' => isset($content['webform']) ? $content['webform'] : '',
		];
	}

	public function render($settings) {
		$build = parent::getBuild($settings);
		$build['#theme'] = 'nce_component';
		if(!\Drupal::moduleHandler()->moduleExists('webform')){
			$build['#content']['webform']['#markup'] = t('Install and configure webform module.');
			return $build;
		}
		if(!empty($settings['content']['webform'])){
			$build['#content']['webform']['#type'] = 'webform';
			$build['#content']['webform']['#webform'] = $settings['content']['webform'];
		}else{
			$build['#content']['webform']['#markup'] = t('Enter webform id.');
		}
		return $build;
	}

}
