<?php

/**
 * @file
 * Contains \Drupal\nc_editor\Plugin\NcEditorComponent\PourcentageComponent.
 */

namespace Drupal\nc_editor\Plugin\NcEditorComponent;

use Drupal\image\Entity\ImageStyle;
use Drupal\nc_editor\NcEditorBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'pourcentage box' Nc Editor component.
 *
 * @NcEditorComponent(
 *   id = "pourcentage",
 *   type = "widget",
 *   name = "Pourcentage",
 *   iconClass = "fa-regular fa-percent",
 * )
 */
class PourcentageComponent extends NcEditorBase {

  public function buildForm(array &$form, FormStateInterface $form_state, array $settings) {
    $content = $settings['content'];

    $form['content']['pourcentage'] = [
      '#title' => "Pourcentage",
      '#type' => 'number',
      '#required' => TRUE,
      '#max' => 100,
      '#min' => 0,
      '#default_value' => isset($content['pourcentage']) ? $content['pourcentage'] : '',
    ];

    $form['content']['texte'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => t('Texte'),
      '#default_value' => isset($content['texte']) ? $content['texte'] : '',
    ];
  }

  public function render($settings) {
    $build = parent::getBuild($settings);

    $content = $settings['content'];
    $build['#theme'] = 'nce_pourcentage';

    //Contenu
    $build['#content']['texte'] = $content['texte'];

    //Pourcentage
    $build['#content']['pourcentage'] = $content['pourcentage'];

    return $build;
  }

}
