<?php

/**
 * @file
 * Contains \Drupal\nc_editor\Plugin\NcEditorComponent\IframeComponent.
 */

namespace Drupal\nc_editor\Plugin\NcEditorComponent;

use Drupal\image\Entity\ImageStyle;
use Drupal\nc_editor\NcEditorBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'iframe box' Nc Editor component.
 *
 * @NcEditorComponent(
 *   id = "iframe",
 *   type = "widget",
 *   name = "Iframe",
 *   iconClass = "fa-regular fa-anchor",
 * )
 */
class IframeComponent extends NcEditorBase
{

    public function buildForm(array &$form, FormStateInterface $form_state, array $settings)
    {
        $content = $settings['content'];

        $form['content']['iframe'] = [
            '#type' => 'url',
            '#required' => true,
            '#title' => t('Iframe'),
            '#default_value' => isset($content['iframe']) ? $content['iframe'] : '',
        ];

        $form['content']['hauteur'] = [
            '#title' => "hauteur",
            '#type' => 'number',
            '#default_value' => isset($content['hauteur']) ? $content['hauteur'] : '',
        ];
    }

    public function render($settings)
    {
        $build = parent::getBuild($settings);

        $content = $settings['content'];
        $build['#theme'] = 'nce_iframe';

        //Contenu

        $build['#content']['iframe'] = $content['iframe'];

        //Légende
        $build['#content']['hauteur'] = $content['hauteur'];

        return $build;
    }

}
