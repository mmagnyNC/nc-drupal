<?php

/**
 * @file
 * Contains \Drupal\nc_editor\Plugin\NcEditorComponent\HeadingComponent.
 */

namespace Drupal\nc_editor\Plugin\NcEditorComponent;

use Drupal\nc_editor\NcEditorBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides a 'heading' Nc Editor component.
 *
 * @NcEditorComponent(
 *   id = "heading",
 *   type = "widget",
 *   name = "Titre",
 *   iconClass = "fa-regular fa-heading",
 * )
 */
class HeadingComponent extends NcEditorBase
{

	public function buildForm(array &$form, FormStateInterface $form_state, array $settings)
	{
		$content = $settings['content'];
		$form['content']['heading'] = [
			'#type' => 'textarea',
			'#required' => true,
			'#title' => "Titre",
			'#default_value' => isset($content['heading']) ? $content['heading'] : '',
		];

		$form['content']['type'] = [
			'#type' => 'select',
			'#required' => true,
			'#title' => "Type de titre",
			'#options' => [
				'h2' => t('Heading') . ' 2',
				'h3' => t('Heading') . ' 3',
				'h4' => t('Heading') . ' 4',
				'h5' => t('Heading') . ' 5',
				'h6' => t('Heading') . ' 6',
			],
			'#default_value' => isset($content['type']) ? $content['type'] : '',
		];
	}

	public function render($settings)
	{
		$build = parent::getBuild($settings);
		if (!empty($settings['content']['heading'])) {
			$heading = $settings['content']['heading'];
			$build['#type'] = 'html_tag';
			$build['#tag'] = $settings['content']['type'];
			$build['#value'] = $heading;
		}
		return $build;
	}

}
