<?php

/**
 * @file
 * Contains \Drupal\nc_editor\Plugin\NcEditorComponent\HighlightComponent.
 */

namespace Drupal\nc_editor\Plugin\NcEditorComponent;

use Drupal\image\Entity\ImageStyle;
use Drupal\nc_editor\NcEditorBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'highlight box' Nc Editor component.
 *
 * @NcEditorComponent(
 *   id = "highlight",
 *   type = "widget",
 *   name = "Mise en lumière",
 *   iconClass = "fa-regular fa-lightbulb-on",
 * )
 */
class HighlightComponent extends NcEditorBase
{

    public function buildForm(array &$form, FormStateInterface $form_state, array $settings)
    {
        $content = $settings['content'];

        $form['content']['highlight'] = [
            '#type' => 'text_format',
            '#required' => true,
            '#title' => "Contenu",
            '#default_value' => isset($content['highlight']['value']) ? $content['highlight']['value'] : '',
            '#format' => isset($content['highlight']['format']) ? $content['highlight']['format'] : 'full_html',
            '#allowed_formats' => unserialize(ALLOWED_TEXT_FORMATS),
            '#prefix' => '<div id="wysiwyg">',
            '#suffix' => '</div>',
        ];
    }

    public function submitForm(array &$form, FormStateInterface $form_state, array $settings) {
        $settings = parent::submitForm($form, $form_state, $settings);
        $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
        $text = $form_state->getValue('content');
        $uuids = _editor_parse_file_uuids($text['highlight']['value']);
        _editor_record_file_usage($uuids, $user);
        return $settings;
    }

    public function render($settings)
    {
        $build = parent::getBuild($settings);

        $content = $settings['content'];
        $build['#theme'] = 'nce_highlight';

        //Contenu
        $build['#content']['highlight'] = $content['highlight']['value'];

        return $build;
    }

}
