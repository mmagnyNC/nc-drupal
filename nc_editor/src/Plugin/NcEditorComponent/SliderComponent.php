<?php

/**
 * @file
 * Contains \Drupal\nc_editor\Plugin\NcEditorComponent\SliderComponent.
 */

namespace Drupal\nc_editor\Plugin\NcEditorComponent;

use Drupal\Component\Utility\Html;
use Drupal\image\Entity\ImageStyle;
use Drupal\nc_editor\NcEditorBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'slider' Nc Editor component.
 *
 * @NcEditorComponent(
 *   id = "slider",
 *   type = "widget",
 *   name = "Carrousel",
 *   iconClass = "fa-regular fa-images",
 * )
 */
class SliderComponent extends NcEditorBase
{

	public function buildForm(array &$form, FormStateInterface $form_state, array $settings)
	{
		$content = $settings['content'];
		$form['content']['sliders'] = [
			'#type' => 'table',
			'#tree' => true,
			'#prefix' => '<div id="sliders-wrapper">',
			'#suffix' => '</div>',
			'#header' => [t('Item'), t('Weight'), "Contenu", t('Actions')],
			'#tabledrag' => [
				[
					'action' => 'order',
					'relationship' => 'sibling',
					'group' => 'slider-weight',
				],
			],
		];
		$sliders = $form_state->getValue(['content', 'sliders']);
		if (empty($sliders) && empty($content['sliders'])) {
			$sliders[] = [
				'weight' => 0,
				'slider' => [],
			];
		}
		elseif (empty($sliders) && !empty($content['sliders'])) {
			$sliders = $content['sliders'];
		}
		uasort($sliders, 'Drupal\Component\Utility\SortArray::sortByWeightElement');
		foreach ($sliders as $delta => $slider) {
			$slider_content = $content['sliders'][$delta]['slider'];
			// TableDrag: Weight column element.
			$form['content']['sliders'][$delta]['#attributes']['class'][] = 'draggable';
			$form['content']['sliders'][$delta]['#weight'] = isset($slider['weight']) ? $slider['weight'] : 0;
			$form['content']['sliders'][$delta]['item'] = ['#plain_text' => ''];
			$form['content']['sliders'][$delta]['weight'] = [
				'#type' => 'weight',
				'#title' => t('Weight for @title', ['@title' => 'slider']),
				'#title_display' => 'invisible',
				'#attributes' => ['class' => ['slider-weight']],
				'#default_value' => isset($slider['weight']) ? $slider['weight'] : 0
			];
			$form['content']['sliders'][$delta]['slider'] = [
				'#type' => 'details',
				'#tree' => true,
				'#collapsible' => true,
				'#title' => !empty($slider_content['title']) ? $slider_content['title'] : t('slider @count', ['@count' => ($delta + 1)])
			];
			$form['content']['sliders'][$delta]['slider']['image'] = [
				'#type' => 'textfield',
				'#title' => t('Image'),
				'#attached' => [
					'library' => [
						'imce/drupal.imce.input',
						'nc_editor/nc_editor.imce',
					],
				],
				'#attributes' => [
					'class' => ['imce-url-input'],
				],
				'#default_value' => isset($slider_content['image']) ? $slider_content['image'] : '',
			];

			$form['content']['sliders'][$delta]['slider']['thumbnail'] = [
				'#type' => 'html_tag',
				'#tag' => 'div',
				'#attributes' => [
					'class' => ['imce_preview'],
				],
			];

			$form['content']['sliders'][$delta]['slider']['title'] = [
				'#title' => "Légende",
				'#type' => 'textfield',
				'#default_value' => isset($slider_content['title']) ? $slider_content['title'] : '',
			];
			if (count($sliders) > 1) {
				$form['content']['sliders'][$delta]['delete'] = [
					'#type' => 'submit',
					'#title' => "Supprimer",
					'#name' => 'delete_' . $delta,
					'#value' => 'Supprimer',
					'#submit' => [get_class($this) . '::ajaxSubmit'],
					'#ajax' => [
						'callback' => get_class($this) . '::addMoreSet',
						'wrapper' => 'sliders-wrapper',
					]
				];
			}
			else {
				$form['content']['sliders'][$delta]['delete'] = [];
			}
		}
		$form['content']['add'] = [
			'#type' => 'submit',
			'#title' => "Ajouter un nouvel élément",
			'#value' => "Ajouter un nouvel élément",
			'#submit' => [get_class($this) . '::ajaxSubmit'],
			'#ajax' => [
				'callback' => get_class($this) . '::addMoreSet',
				'wrapper' => 'sliders-wrapper',
			]
		];

		/* OPTIONS */
		$options = isset($settings['options']) ? $settings['options'] : [];
		$form['options'] = [
			'#type' => 'details',
			'#title' => t('Options'),
			'#group' => 'settings',
			'#tree' => true
		];
		$form['options']['indicators'] = [
			'#type' => 'checkbox',
			'#title' => "Afficher les points",
			'#default_value' => !empty($options['indicators']) ? $options['indicators'] : 0,
		];
		$form['options']['controls'] = [
			'#type' => 'checkbox',
			'#title' => "Afficher les contrôles",
			'#default_value' => !empty($options['controls']) ? $options['controls'] : 0,
		];
		$form['options']['interval'] = [
			'#type' => 'number',
			'#title' => "Interval",
			'#default_value' => !empty($options['interval']) ? $options['interval'] : 3000,
		];
		$form['options']['number'] = [
			'#type' => 'number',
			'#title' => "Nombre d'image à afficher",
			'#default_value' => !empty($options['number']) ? $options['number'] : 1,
			'#attributes' => [
				'min' => 1,
				'max' => 4,
			],
		];
	}

	public static function addMoreSet(array &$form, FormStateInterface $form_state) {
		return $form['content']['sliders'];
	}

	/**
	 * {@inheritdoc}
	 */
	public static function ajaxSubmit(array &$form, FormStateInterface $form_state) {
		$sliders = $form_state->getValue('content');
		$parents = $form_state->getTriggeringElement();
		$parents = $parents['#parents'];
		if (isset($parents[1]) && $parents[1] == 'add') {
			$sliders['sliders'][] = [
				'weight' => 0,
				'slider' => [],
			];
		}
		if (isset($parents[3]) && $parents[3] == 'delete') {
			unset($sliders['sliders'][$parents[2]]);
		}
		$form_state->setValue('content', $sliders);
		$form_state->setRebuild(TRUE);
	}

	public function submitForm(array &$form, FormStateInterface $form_state, array $settings) {
		$settings = parent::submitForm($form, $form_state, $settings);
		$settings['options'] = $form_state->getValue('options');
		return $settings;
	}

	public function render($settings)
	{
		$build = parent::getBuild($settings);
		if(!empty($settings['content']['sliders'])){
			uasort($settings['content']['sliders'], 'Drupal\Component\Utility\SortArray::sortByWeightElement');
			$build['#theme'] = 'nce_sliders';
			$build['#content']['sliders_id'] = Html::getUniqueId('nce-sliders');
			foreach ($settings['content']['sliders'] as $key => $slider) {
				$build['#content']['sliders'][$key]['id'] = Html::getUniqueId('nce-sliders');
				if(!empty($slider['slider']['image'])){
					$image = str_replace('/sites/default/files', 'public:/',  urldecode($slider['slider']['image']));
					$build['#content']['sliders'][$key]['image'] = ImageStyle::load('nc_editor_slider')->buildUrl($image, true);

					$build['#content']['sliders'][$key]['title'] = $slider['slider']['title'];
				}
			}

			unset($settings['options']['format']);
			$build['#settings'] = $settings['options'];

			$build['#attributes']['class'][] = 'widget-slick';
		}

		return $build;
	}
}
