<?php

/**
 * @file
 * Contains \Drupal\nc_editor\Plugin\NcEditorComponent\RichTextComponent.
 */

namespace Drupal\nc_editor\Plugin\NcEditorComponent;

use Drupal\nc_editor\NcEditorBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'wysiwyg' Nc Editor component.
 *
 * @NcEditorComponent(
 *   id = "wysiwyg",
 *   type = "widget",
 *   name = @Translation("WYSIWYG"),
 *   iconClass = "fa-regular fa-edit",
 * )
 */
class WysiwygComponent extends NcEditorBase {

	public function buildForm(array &$form, FormStateInterface $form_state, array $settings) {
		$content = $settings['content'];
		$form['content']['wysiwyg'] = [
			'#type' => 'text_format',
			'#required' => true,
			'#title' => t('WYSIWYG'),
			'#default_value' => isset($content['wysiwyg']['value']) ? $content['wysiwyg']['value'] : '',
			'#format' => isset($content['wysiwyg']['format']) ? $content['wysiwyg']['format'] : 'full_html',
			'#allowed_formats' => unserialize(ALLOWED_TEXT_FORMATS),
			'#prefix' => '<div id="wysiwyg">',
			'#suffix' => '</div>',
		];
	}

	public function submitForm(array &$form, FormStateInterface $form_state, array $settings) {
		$settings = parent::submitForm($form, $form_state, $settings);
		$user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
		$text = $form_state->getValue('content');
		$uuids = _editor_parse_file_uuids($text['wysiwyg']['value']);
		_editor_record_file_usage($uuids, $user);
		return $settings;
	}

	public function render($settings) {
		$build = parent::getBuild($settings);
		$wysiwyg = $settings['content']['wysiwyg'];
		$build['#theme'] = 'nce_component';
		$build['#content']['content']['#type'] = 'processed_text';
		$build['#content']['content']['#format'] = $wysiwyg['format'];
		$build['#content']['content']['#text'] = $wysiwyg['value'];
		return $build;
	}

}
