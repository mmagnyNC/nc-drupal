<?php

/**
 * @file
 * Contains \Drupal\nc_editor\Plugin\NcEditorComponent\ImageComponent.
 */

namespace Drupal\nc_editor\Plugin\NcEditorComponent;

use Drupal\image\Entity\ImageStyle;
use Drupal\nc_editor\NcEditorBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'image box' Nc Editor component.
 *
 * @NcEditorComponent(
 *   id = "image",
 *   type = "widget",
 *   name = @Translation("Image"),
 *   iconClass = "fa-regular fa-image",
 * )
 */
class ImageComponent extends NcEditorBase
{

	public function buildForm(array &$form, FormStateInterface $form_state, array $settings)
	{
		$content = $settings['content'];
		$form['content']['image'] = [
			'#type' => 'textfield',
			'#title' => t('Image'),
			'#attached' => [
				'library' => [
					'imce/drupal.imce.input',
					'nc_editor/nc_editor.imce',
				],
			],
			'#attributes' => [
				'class' => ['imce-url-input'],
			],
			'#default_value' => isset($content['image']) ? $content['image'] : '',
		];

		$form['content']['thumbnail'] = [
			'#type' => 'html_tag',
			'#tag' => 'div',
			'#attributes' => [
				'class' => ['imce_preview'],
			],
		];

		$form['content']['url'] = [
			'#title' => t('URL'),
			'#type' => 'url',
			'#default_value' => (isset($content['url'])) ? static::getUriAsDisplayableString($content['url']) : NULL,
			'#element_validate' => [[get_called_class(), 'validateUriElement']],
			'#description' => 'Pour déposer un fichier, ouvrez le "<u><a href="/imce" target="_blank" title="Navigateur de fichiers">Navigateur de fichiers</a></u>"'
		];

		$form['content']['target'] = [
			'#title' => t('Target'),
			'#type' => 'select',
			'#empty_option' => t(' - Select - '),
			'#options' => [
				'_blank' => 'Lien externe (_blank)',
			],
			'#default_value' => isset($content['target']) ? $content['target'] : '',
		];
	}

	public function render($settings)
	{
		$build = parent::getBuild($settings);

		$content = $settings['content'];
		$build['#theme'] = 'nce_image';

		//IMAGE
		$build['#content']['image'] = $content['image'];

		$build['#attributes']['class'][] = 'figure';

		if(!empty($content['url'])){
			if(strpos($content['url'], 'internal:/') === 0){
				$content['url'] = str_replace('internal:/', '/', $content['url']);
			}
			$build['#content']['url'] = $content['url'];

			if (!empty($content['target'])) {
				$build['#content']['target'] = $content['target'];
			}
		}

		return $build;
	}

}
