<?php

namespace Drupal\nc_editor\Plugin\Editor;

use Drupal\Core\Form\FormStateInterface;
use Drupal\editor\Entity\Editor;
use Drupal\editor\Plugin\EditorBase;
use Drupal\nc_editor\NcEditorConfig;

/**
 * Defines a CKEditor-based text editor for Drupal.
 *
 * @Editor(
 *   id = "nc_editor",
 *   label = @Translation("Nc Editor"),
 *   supports_content_filtering = FALSE,
 *   supports_inline_editing = FALSE,
 *   supported_element_types = {
 *     "textarea"
 *   },
 *   is_xss_safe = TRUE
 * )
 */
class NcEditor extends EditorBase
{

	public function getJSSettings(Editor $editor)
	{
		$settings = $editor->getSettings();
		// Get all the Nc Editor components definitions.
		$veConfig = new NcEditorConfig();
		$settings['buttons'] = $veConfig->getComponents();
		$settings['pluginSettings'] = $veConfig->getJSSettings();
		$settings['basePath'] = \Drupal\Core\Url::fromRoute('<front>')->toString();
		$settings['editorPath'] = drupal_get_path('module', 'nc_editor');
		$form = \Drupal::formBuilder()->getForm('Drupal\nc_editor\Form\ComponentUtilityForm');
		$settings['componentUtilityForm'] = \Drupal::service('renderer')->render($form);
		$settings['componentFormID'] = NCE_COMPONENT_FORM_WRAP;
		$settings['componentUtilityFormWrap'] = NCE_COMPONENT_UTILITY_FORM;
		$settings['nce_config']['frontend'] = \Drupal::config('nc_editor.config')->get('frontend');
		$settings['nce_config']['components'] = \Drupal::config('nc_editor.config')->get('components');
		return $settings;
	}

	public function getLibraries(Editor $editor)
	{
		$veConfig = new NcEditorConfig();
		return $veConfig->getLibraries();
	}

	public function settingsForm(array $form, FormStateInterface $form_state, Editor $editor)
	{
		$settings = $editor->getSettings();
		return $form;
	}

}
