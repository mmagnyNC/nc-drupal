<?php
define('NCE_COMPONENT_FORM_WRAP', 'nc-editor-component-form-wrapper');
define('NCE_COMPONENT_UTILITY_FORM', 'nc-editor-component-utility-form');
define('YOUTUBE_REGX', "/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/");
define('VIMEO_REGX', "/(https?:\/\/)?(www\.)?(player\.)?vimeo\.com\/([a-z]*\/)*([‌​0-9]{6,11})[?]?.*/");
define('ALLOWED_TEXT_FORMATS', serialize([
	'full_html',
]));

/**
 * Implements hook_field_widget_form_alter()
 * @param type $element
 * @param type $form_state
 * @param type $context
 */
function nc_editor_field_widget_form_alter(&$element, &$form_state, $context)
{
	// Add a css class to widget form elements for all fields of type text_format.
	if (isset($element['#type']) && $element['#type'] == 'text_format') {
		// Be sure not to overwrite existing attributes.
		$element['nc_editor'] = [
			'#type' => 'hidden',
			'#attributes' => [
				'class' => [
					'nc-editor-config'
				]
			]
		];
	}
}

/**
 * Alter the list of attached files for the editor depending on the fields conf.
 *
 * @param $attachments
 *   #attached array returned by the editor attachments callback.
 * @param $editor_id
 *   ID of the currently used editor.
 */
function nc_editor_quickedit_editor_attachments_alter(&$attachments, $editor_id)
{
	if ($editor_id === 'nc_editor') {
		$attachments['library'][] = ['core', 'jquery.form'];
		$attachments['library'][] = ['core', 'jquery.ui.sortable'];
		$attachments['library'][] = ['core', 'drupal.vertical-tabs'];
		$attachments['library'][] = ['core', 'drupal.dialog.ajax'];
		$attachments['library'][] = ['core', 'drupal.dialog'];
		$attachments['library'][] = ['nc_editor', 'drupal.nc_editor'];
		$attachments['library'][] = ['nc_editor', 'nc_editor.fontawesome'];
	}
}

/**
 * Implements hook_theme().
 */
function nc_editor_theme($existing, $type, $theme, $path)
{
	$variables = [
		'content' => NULL,
		'attributes' => NULL,
		'settings' => NULL,
	];
	$suggestions = [];
	$suggestions['nce_component'] = [
		'variables' => $variables,
	];
	$suggestions['nce_button'] = [
		'variables' => $variables,
		'template' => 'nce-component--widget-button'
	];
	$suggestions['nce_embeded_video'] = [
		'variables' => $variables,
		'template' => 'nce-component--widget-embeded-video'
	];
	$suggestions['nce_accordions'] = [
		'variables' => $variables,
		'template' => 'nce-component--widget-accordions'
	];
	$suggestions['nce_image'] = [
		'variables' => $variables,
		'template' => 'nce-component--widget-image'
	];
	$suggestions['nce_sliders'] = [
		'variables' => $variables,
		'template' => 'nce-component--widget-sliders'
	];
	$suggestions['nce_blockquote'] = [
		'variables' => $variables,
		'template' => 'nce-component--widget-blockquote'
	];
    $suggestions['nce_formations'] = [
        'variables' => $variables,
        'template' => 'nce-component--widget-formations'
    ];
    $suggestions['nce_iframe'] = [
        'variables' => $variables,
        'template' => 'nce-component--widget-iframe'
    ];
    $suggestions['nce_highlight'] = [
        'variables' => $variables,
        'template' => 'nce-component--widget-highlight'
    ];
    $suggestions['nce_pourcentage'] = [
        'variables' => $variables,
        'template' => 'nce-component--widget-pourcentage'
    ];
    $suggestions['nce_html'] = [
        'variables' => $variables,
        'template' => 'nce-component--widget-html'
    ];

	return $suggestions;
}

/**
 * Implements hook_theme_suggestions_HOOK().
 *  Add template suggestions.
 */
function nc_editor_theme_suggestions_nce_component($variables)
{
	$suggestions = [];
	$settings = $variables['settings'];
	$suggestions[] = 'nce_component__' . $settings['type'];
	$suggestions[] = 'nce_component__' . $settings['type'] . '_' . $settings['id'];
	return $suggestions;
}

function nc_editor_preprocess_node(&$variables) {
	$variables['#attached']['library'][] =  'nc_editor/nc_editor.front';
}
