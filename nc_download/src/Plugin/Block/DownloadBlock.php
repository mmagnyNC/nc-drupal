<?php
namespace Drupal\nc_download\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\file\Entity\File;
/**
 * Provides a 'Download Files' Block.
 *
 * @Block(
 *   id = "nc_download",
 *   admin_label = @Translation("Download Files - Block"),
 * )
 */

class DownloadBlock extends BlockBase implements ContainerFactoryPluginInterface {
	/**
	 * The current route match.
	 *
	 * @var \Drupal\Core\Routing\RouteMatchInterface
	 */
	protected $routeMatch;

	/**
	 * Constructs a new BookNavigationBlock instance.
	 *
	 * @param array $configuration
	 *   A configuration array containing information about the plugin instance.
	 * @param string $plugin_id
	 *   The plugin_id for the plugin instance.
	 * @param mixed $plugin_definition
	 *   The plugin implementation definition.
	 * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
	 *   The current route match.
	 */
	public function __construct(array $configuration, $plugin_id, $plugin_definition, RouteMatchInterface $route_match) {
		parent::__construct($configuration, $plugin_id, $plugin_definition);

		$this->routeMatch = $route_match;
	}

	/**
	 * {@inheritdoc}
	 */
	public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
		return new static(
			$configuration,
			$plugin_id,
			$plugin_definition,
			$container->get('current_route_match')
		);
	}

	/**
	 * {@inheritdoc}
	 */
	public function blockForm($form, FormStateInterface $form_state) {
		$form = parent::blockForm($form, $form_state);
		$config = $this->getConfiguration();

		$form['nc_download'] = [
			'#type' => 'fieldset',
			'#title' => "Configuration",
			'#tree' => TRUE,

			'field' => [
				'#type' => 'textfield',
				'#title' => $this->t('Field file name'),
				'#default_value' => isset($config['nc_download_field']) ? $config['nc_download_field'] : null,
			],
		];
		return $form;
	}

	/**
	 * {@inheritdoc}
	 */
	public function blockSubmit($form, FormStateInterface $form_state) {
		parent::blockSubmit($form, $form_state);
		$values = $form_state->getValues();

		$this->configuration['nc_download_field'] = $values['nc_download']['field'];
	}

	/**
	 * {@inheritdoc}
	 */
	public function build() {
		/** @var \Drupal\node\NodeInterface $node */
		$node = $this->routeMatch->getParameter('node');

		if (!($node instanceof NodeInterface)) {
			return [];
		}

		$data = [];

		$config = $this->getConfiguration();
		$field = $config['nc_download_field'];

		if ($node->hasField($field) && count($node->get($field)->getValue()) > 0) {
			foreach ($node->get($field)->getValue() as $fichier) {
				$file = File::load($fichier['target_id']);
				if (!empty($file)) {
					$data[] = [
						'label' => (!empty($fichier['description'])) ? $fichier['description'] : str_replace("_", " ", $file->getFilename()),
						'url' => \Drupal::service('file_url_generator')->generateAbsoluteString($file->getFileUri()),
						'icon' => $this->getIconContentType($file->getMimeType()),
					];
				}
			}
		}

		if(count($data) > 0){
			return [
				'#theme' => 'nc_download_list',
				'#data' => $data,
			];
		}

		return [];
	}

	/**
	 * {@inheritdoc}
	 */
	public function getCacheTags() {
		// With this when your node change your block will rebuild.
		if ($node = \Drupal::routeMatch()->getParameter('node')) {
			// If there is node add its cachetag.
			return Cache::mergeTags(parent::getCacheTags(), ['node:' . $node->id()]);
		}
		else {
			// Return default tags instead.
			return parent::getCacheTags();
		}
	}

	public function getCacheContexts() {
		// Every new route this block will rebuild.
		return Cache::mergeContexts(parent::getCacheContexts(), ['route']);
	}

	private function getIconContentType($type){
		$mime_types = array(

			'text/plain' => 'file-text-o',
			'text/html' => 'file-code-o',
			'text/css' => 'file-code-o',
			'application/javascript' => 'file-code-o',
			'application/json' => 'file-code-o',
			'application/xml' => 'file-code-o',
			'application/x-shockwave-flash' => 'file-code-o',
			'video/x-flv' => 'file-video-o',

			// images
			'image/png' => 'file-image-o',
			'image/jpeg' => 'file-image-o',
			'image/gif' => 'file-image-o',
			'image/bmp' => 'file-image-o',
			'image/vnd.microsoft.icon' => 'file-image-o',
			'image/tiff' => 'file-image-o',
			'image/svg+xml' => 'file-image-o',

			// archives
			'application/zip' => 'file-archive-o',
			'application/x-rar-compressed' => 'file-archive-o',
			'application/x-msdownload' => 'file-code-o',
			'application/vnd.ms-cab-compressed' => 'file-archive-o',

			// audio/video
			'audio/mpeg' => 'file-audio-o',
			'video/quicktime' => 'file-video-o',

			// adobe
			'application/pdf' => 'file-pdf-o',
			'image/vnd.adobe.photoshop' => 'file-image-o',
			'application/postscript' => 'file-image-o',

			// ms office
			'application/msword' => 'file-word-o',
			'application/rtf' => 'file-word-o',
			'application/vnd.ms-excel' => 'file-excel-o',
			'application/vnd.ms-powerpoint' => 'file-powerpoint-o',

			// open office
			'application/vnd.oasis.opendocument.text' => 'file-word-o',
			'application/vnd.oasis.opendocument.spreadsheet' => 'file-excel-o',
		);

		if(isset($mime_types[$type]))
			return $mime_types[$type];
		else
			return 'file-code-o';
	}
}
