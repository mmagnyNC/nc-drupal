jQuery(document).ready(function($) {
    if($('#modal-popin').length > 0){
        $("#modal-popin").modal("show");
        //Modal has been shown, now set a cookie so it never comes back
        $("#modal-popin .close").click(function () {
            $("#modal-popin").modal("hide");
        });
    }
});