<?php

namespace Drupal\nc_popin\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form that configures forms module settings.
 */
class NcPopinSettingsForm extends ConfigFormBase {

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'nc_popin_admin_settings';
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames() {
        return [
            'nc_popin.settings',
        ];
    }

    /**
     *
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $config = $this->config('nc_popin.settings');

        $form['type'] = [
            '#type' => 'select',
            '#options' => [
                'off' => $this->t('Off'),
                'unique' => $this->t('Unique'),
                'hour' => $this->t('Every X hours'),
                'all' => $this->t('Each page load'),
            ],
            '#title' => $this->t('Pop-in type'),
            '#default_value' => $config->get('type'),
            '#description' => $this->t('Choose the pop-in type for the website'),
        ];

        $form['date'] = [
            '#type' => 'fieldset',
            '#title' => t('Period'),
            '#tree' => TRUE,
        ];

        $form['date']['start'] = [
            '#type' => 'date',
            '#title' => $this->t('Start date'),
            '#default_value' => $config->get('date_start'),
            '#description' => $this->t('Choose the start date'),
        ];

        $form['date']['end'] = [
            '#type' => 'date',
            '#title' => $this->t('End date'),
            '#default_value' => $config->get('date_end'),
            '#description' => $this->t('Choose the end date'),
        ];

        $form['hour'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Hours'),
            '#default_value' => (!empty($config->get('hour'))) ?  $config->get('hour') : '0',
        ];

        $form['title'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Title'),
            '#default_value' => $config->get('title'),
        ];

        $form['message'] = [
            '#type' => 'text_format',
            '#title' => $this->t('Message'),
            '#format' => 'full_html',
            '#default_value' => $config->get('message'),
            '#description' => $this->t('Choose the start date'),
        ];

        return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $values = $form_state->getValues();
        $this->config('nc_popin.settings')
            ->set('type', $values['type'])
            ->set('date_start', $values['date']['start'])
            ->set('date_end', $values['date']['end'])
            ->set('hour', $values['hour'])
            ->set('title', $values['title'])
            ->set('message', $values['message']['value'])
            ->save();

        parent::submitForm($form, $form_state);
    }

}