<?php

namespace Drupal\nc_twitter\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\nc_twitter\TwitterAPIExchange;

/**
 * Provides a 'Twitter Feed' Block.
 *
 * @Block(
 *   id = "nc_twitter_block",
 *   admin_label = @Translation("Twitter Feed - Block"),
 * )
 */
class NcTwitterBlock extends BlockBase
{
    /**
     * {@inheritdoc}
     */
    public function blockForm($form, FormStateInterface $form_state)
    {
        $form = [
            'markup' => [
                '#type' => 'markup',
                '#markup' => t('<a href="@url">Create a twitter app on the twitter developer site</a>', ['@url' => 'https://dev.twitter.com/apps/']),
            ],
            'tweets_username' => [
                '#type' => 'textfield',
                '#title' => $this->t('Twitter username'),
                '#description' => $this->t(''),
                '#default_value' => isset($this->configuration['tweets_username']) ? $this->configuration['tweets_username'] : 'netcom',
            ],
            'tweets_limit' => [
                '#type' => 'textfield',
                '#title' => $this->t('Limit'),
                '#description' => $this->t(''),
                '#default_value' => isset($this->configuration['tweets_limit']) ? $this->configuration['tweets_limit'] : 3,
            ],
            'access_token' => [
                '#type' => 'textfield',
                '#title' => $this->t('Access token'),
                '#description' => $this->t(''),
                '#default_value' => isset($this->configuration['access_token']) ? $this->configuration['access_token'] : '',
            ],
            'token_secret' => [
                '#type' => 'textfield',
                '#title' => $this->t('Token secret'),
                '#description' => $this->t(''),
                '#default_value' => isset($this->configuration['token_secret']) ? $this->configuration['token_secret'] : '',
            ],
            'consumer_key' => [
                '#type' => 'textfield',
                '#title' => $this->t('Consumer key'),
                '#description' => $this->t(''),
                '#default_value' => isset($this->configuration['consumer_key']) ? $this->configuration['consumer_key'] : '',
            ],
            'consumer_secret' => [
                '#type' => 'textfield',
                '#title' => $this->t('Consumer secret'),
                '#description' => $this->t(''),
                '#default_value' => isset($this->configuration['consumer_secret']) ? $this->configuration['consumer_secret'] : '',
            ],
        ];
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function blockSubmit($form, FormStateInterface $form_state)
    {
        $this->configuration['access_token'] = $form_state->getValue('access_token');
        $this->configuration['token_secret'] = $form_state->getValue('token_secret');
        $this->configuration['consumer_key'] = $form_state->getValue('consumer_key');
        $this->configuration['consumer_secret'] = $form_state->getValue('consumer_secret');
        $this->configuration['tweets_username'] = $form_state->getValue('tweets_username');
        $this->configuration['tweets_limit'] = $form_state->getValue('tweets_limit');
    }

    /**
     * {@inheritdoc}
     */
    public function build()
    {
        $build = [];

        $url = 'https://api.twitter.com/1.1/search/tweets.json';
        $parameter_search = "q";
       
        $fieldTwitter = $this->configuration['tweets_username'];
        if (!empty($fieldTwitter)) {

            if (substr($fieldTwitter,0,1) == "@" ) {
                $parameter_search = "screen_name";
                $fieldTwitter = substr($fieldTwitter,1);
                $url = "https://api.twitter.com/1.1/statuses/user_timeline.json";
            }

            $settings = [
                'oauth_access_token' => $this->configuration['access_token'],
                'oauth_access_token_secret' => $this->configuration['token_secret'],
                'consumer_key' => $this->configuration['consumer_key'],
                'consumer_secret' => $this->configuration['consumer_secret']
            ];

            if(!empty($settings['oauth_access_token']) && !empty($settings['oauth_access_token_secret']) && !empty($settings['consumer_key']) && !empty($settings['consumer_secret'])){
                $getField = '?'.$parameter_search.'=' . $fieldTwitter . '&count=' . $this->configuration['tweets_limit'];
                $requestMethod = 'GET';
                $twitter = new TwitterAPIExchange($settings);

                $response = $twitter->setGetfield($getField)
                    ->buildOauth($url, $requestMethod)
                    ->performRequest();
                if ($response) {
                    $tweets = json_decode($response);
                    if (isset($tweets->statuses)){
                        $tweets_to_send = $tweets->statuses;
                    }else{
                        $tweets_to_send = $tweets;
                    }

                    if(!empty($tweets_to_send)){
                        $build = [
                            '#theme' => 'twitter',
                            '#tweets' => $tweets_to_send,
                            '#title' => $fieldTwitter,
                        ];
                    }
                }
            }
        }
		
        return $build;
    }
}
