jQuery(document).ready(function($) {
    //Popup - Default config
    var popupCenter = function(url, title, width, height){
        var popupWidth = width || 640;
        var popupHeight = height || 320;
        var windowLeft = window.screenLeft || window.screenX;
        var windowTop = window.screenTop || window.screenY;
        var windowWidth = window.innerWidth || document.documentElement.clientWidth;
        var windowHeight = window.innerHeight || document.documentElement.clientHeight;
        var popupLeft = windowLeft + windowWidth / 2 - popupWidth / 2 ;
        var popupTop = windowTop + windowHeight / 2 - popupHeight / 2;
        var popup = window.open(url, title, 'scrollbars=yes, width=' + popupWidth + ', height=' + popupHeight + ', top=' + popupTop + ', left=' + popupLeft);
        popup.focus();
        return true;
    };

    //Récupération des informations de la page en cours de consultation
    var url = window.location.href;
    var title = document.title;

    //Bouton Twitter
    $('#shareTwitter').on('click', function(e){
        e.preventDefault();
        var shareUrl = "https://twitter.com/intent/tweet?text=" + encodeURIComponent(title) +
            "&url=" + encodeURIComponent(url);
        popupCenter(shareUrl, "Partager sur Twitter");
    });

    //Bouton Facebook
    $('#shareFacebook').on('click', function(e){
        e.preventDefault();
        var shareUrl = "https://www.facebook.com/sharer.php?u=" + encodeURIComponent(url);
        popupCenter(shareUrl, "Partager sur Facebook");
    });

    //Bouton Google
    $('#shareGoogle').on('click', function(e){
        e.preventDefault();
        var shareUrl = "https://plus.google.com/share?url=" + encodeURIComponent(url);
        popupCenter(shareUrl, "Partager sur Google+");
    });

    //Bouton LinkedIn
    $('#shareLinkedin').on('click', function(e){
        e.preventDefault();
        var shareUrl = "https://www.linkedin.com/shareArticle?url=" + encodeURIComponent(url);
        popupCenter(shareUrl, "Partager sur Linkedin");
    });

    //Bouton Pinterest
    $('#sharePinterest').on('click', function(e){
        e.preventDefault();
        var shareUrl = "http://www.pinterest.com/pin/create/button/?url=" + encodeURIComponent(url) +
            "&description=" + encodeURIComponent(document.title) +
            "&media=" + encodeURIComponent(url);
        popupCenter(shareUrl, "Partager sur Pinterest");
    });
});
