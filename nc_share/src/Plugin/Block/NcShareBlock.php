<?php

namespace Drupal\nc_share\Plugin\Block;

use Drupal\Core\Url;
use Drupal\Core\Block\BlockBase;
use Symfony\Cmf\Component\Routing\RouteObjectInterface;

/**
 * Provides a 'Share Button' Block.
 *
 * @Block(
 *   id = "nc_share_block",
 *   admin_label = @Translation("Share Button - Block"),
 * )
 */
class NcShareBlock extends BlockBase
{

	/**
	 * {@inheritdoc}
	 */

	public function build()
	{
		$rows = [];
		$config = \Drupal::config('nc_share.settings');

		$request = \Drupal::request();
		$title = '';
		if ($route = $request->attributes->get(RouteObjectInterface::ROUTE_OBJECT)) {
			$title = \Drupal::service('title_resolver')->getTitle($request, $route);
			if(!is_string($title)){
				$title = $title['#markup'];
			}
		}

		$url = Url::fromRoute('<current>', [], ['absolute' => 'true'])->toString();

		$subject = $config->get('subject');
		$message = $config->get('message');

		$message = str_replace("[share_link]", $url, $message);
		$message = str_replace("[name]", $title, $message);

		$message = str_replace("\r\n\r\n", "%0D%0A%0D", $message);
		$message = str_replace("\r\n", "%0D", $message);

		if (!is_null($url)) {
			$rows = [
				'facebook' => [
					'active' => $config->get('facebook'),
				],
				'twitter' => [
					'active' => $config->get('twitter'),
				],
				'linkedin' => [
					'active' => $config->get('linkedin'),
				],
				'pinterest' => [
					'active' => $config->get('pinterest'),
				],
				'email' => [
					'active' => $config->get('mail'),
					'sujet' => $subject,
					'message' => $message,
				],
				'rss' => [
					'active' => $config->get('rss'),
					'url' => $config->get('url_rss'),
				],
				'print' => [
					'active' => $config->get('print'),
				],
			];
		}

		if (count($rows) > 0) {
			$build = [
				'#theme' => 'nc_share_buttons',
				'#data' => $rows,
				'#cache' => [
					'contexts' => ['url.query_args'],
				],
				'#attached' => [
					'library' => [
						'nc_share/share',
					],
				],
			];
		} else {
			$build = [];
		}

		return $build;
	}
}
