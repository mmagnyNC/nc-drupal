<?php
namespace Drupal\nc_size\Plugin\Block;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'A+A-' Block.
 *
 * @Block(
 *   id = "nc_size",
 *   admin_label = @Translation("A+A- - Block"),
 * )
 */

class SizeBlock extends BlockBase {
    /**
     * {@inheritdoc}
     */
    public function blockForm($form, FormStateInterface $form_state) {
        $form = parent::blockForm($form, $form_state);
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function blockSubmit($form, FormStateInterface $form_state) {
        parent::blockSubmit($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function build()
    {
        $build = [
            '#theme' => 'nc_size',
            '#attached' => [
                'library' => [
                    'nc_size/size',
                ],
            ],
        ];

        return $build;
    }
}
